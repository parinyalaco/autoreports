$(document).ready(function () {
    $('.calsum').on('change',function(){
        recalsummary();
    });
});

function recalsummary(){
    tbase = $('#t_base').val();
    lbase = $('#l_base').val();
    console.log(tbase + ' ' + lbase);
    theight = $('#t_height').val();
    lheight = $('#l_height').val();
    console.log(theight + ' ' + lheight);
    texcess = $('#t_excess').val();
    lexcess = $('#l_excess').val();
    
    texcess1 = $('#t_excess1').val();
    lexcess1 = $('#l_excess1').val();
    
    texcess2 = $('#t_excess2').val();
    lexcess2 = $('#l_excess2').val();

    console.log(theight + ' ' + lheight);
    if((tbase > 0 && theight >0 ) || ( texcess > 0 || texcess1 > 0 || texcess2 > 0 )){
        ttotal = 0;        
        if(tbase > 0 && theight >0 ){
            ttotal += parseInt(tbase * theight);
        }        
        if(texcess > 0){
            ttotal += parseInt(texcess)             
        }
        if(texcess1 > 0){
            ttotal += parseInt(texcess1)             
        }
        if(texcess2 > 0){
            ttotal += parseInt(texcess2)             
        }
        $('#t_total').val(ttotal);
    }else{
         $('#t_total').val(0);
    }

    if((lbase > 0 && lheight >0) || ( lexcess > 0 || lexcess1 > 0 || lexcess2 > 0)){
        ltotal = 0;
        if(lbase > 0 && lheight >0){
            ltotal += parseInt(lbase * lheight); 
        }
        if(lexcess > 0){            
            ltotal += parseInt(lexcess);
        }
        if(lexcess1 > 0){            
            ltotal += parseInt(lexcess1);
        }
        if(lexcess2 > 0){            
            ltotal += parseInt(lexcess2);
        }
        $('#l_total').val(ltotal);
    }else{
         $('#l_total').val(0);
    }

    $('#all_total').val(parseInt($('#t_total').val()) + parseInt($('#l_total').val()));
}
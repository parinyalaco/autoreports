<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
    use InfluxDB2\Client;
use InfluxDB2\Model\WritePrecision;
use InfluxDB2\Point;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


route::get('/test',function(){


# You can generate a Token from the "Tokens Tab" in the UI
$token = 'KBWDG9DYy_GvG1bjIaJDVjcO9ZM4r1GNsc9ygc0gK4iY37PZyJiLB1ZGorKo2lZl9MTNs47zoJMRLYQSf-GXug==';
$org = 'demo';
$bucket = 'demo-bucket';

$client = new Client([
    "url" => "http://159.223.48.137:8086",
    "token" => $token,
]);
$query = "from(bucket: \"{$bucket}\") |> range(start: -1m) |> filter(fn: (r) => r[\"_measurement\"] == \"temperature\") |> filter(fn: (r) => r[\"_field\"] == \"value\") |> mean()";
$tables = $client->createQueryApi()->query($query, $org);
$data = [];
foreach ($tables as $item) {
        $data[$item->records[0]->values['_measurement']][] = $item->records[0]->values;
}

return $data;
});

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TempMapColumnsController;
use App\Http\Controllers\SetTimeFlagsContoller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TempsController;
use App\Http\Controllers\SensorsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('qc')->group(function () {
    Route::resource('temp_map_columns', TempMapColumnsController::class);
    Route::resource('temps', TempsController::class);
    Route::resource('sensors', SensorsController::class);
    Route::get('temps/excel/{date}', [TempsController::class, 'excel'])->name('temps.excel');
    Route::resource('set_time_flags', SetTimeFlagsContoller::class);
    Route::get('set_time_flags/clone/{id}', [SetTimeFlagsContoller::class, 'clone'])->name('set_time_flags.clone');
});
Route::resource('user', UserController::class);


Route::group(['namespace' => 'App\Http\Controllers'], function()
{   
    /**
     * Home Routes
     */
    Route::get('/', 'HomeController@index')->name('home.index');

    Route::group(['middleware' => ['guest']], function() {
        /**
         * Register Routes
         */
        Route::get('/register', 'RegisterController@show')->name('register.show');
        Route::post('/register', 'RegisterController@register')->name('register.perform');

        /**
         * Login Routes
         */
        Route::get('/login', 'LoginController@show')->name('login.show');
        Route::post('/login', 'LoginController@login')->name('login.perform');

    });

    Route::group(['middleware' => ['auth']], function() {
        /**
         * Logout Routes
         */
        Route::get('/logout', 'LogoutController@perform')->name('logout.perform');
    });
});
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220124 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'temp_map_columns',
            function (Blueprint $table) {
                $table->string('displaycond')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('temp_map_columns', 'displaycond')) {
            Schema::table('temp_map_columns', function (Blueprint $table) {
                $table->dropColumn('displaycond');
            });
        }
    }
}

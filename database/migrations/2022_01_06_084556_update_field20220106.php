<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateField20220106 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'temp_data_d_s',
            function (Blueprint $table) {
                $table->string('status', 50)->default('Active');
            }
        );

        Schema::table(
            'temp_data_m_s',
            function (Blueprint $table) {
                $table->string('sheet')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('temp_data_m_s', 'status')) {
            Schema::table('temp_data_m_s', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }

        if (Schema::hasColumn('temp_map_columns', 'sheet')) {
            Schema::table('temp_map_columns', function (Blueprint $table) {
                $table->dropColumn('sheet');
            });
        }
    }
}

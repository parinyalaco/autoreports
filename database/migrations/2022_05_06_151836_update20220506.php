<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220506 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'sensors',
            function (Blueprint $table) {
                $table->string('conditions',100)->nullable();
                $table->string('conditions2',100)->nullable();
                $table->string('displaycond',100)->nullable();
                $table->string('show',50)->nullable();
                $table->integer('seq')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sensors', 'conditions')) {
            Schema::table('sensors', function (Blueprint $table) {
                $table->dropColumn('conditions');
            });
        }
        if (Schema::hasColumn('sensors', 'conditions2')) {
            Schema::table('sensors', function (Blueprint $table) {
                $table->dropColumn('conditions2');
            });
        }
        if (Schema::hasColumn('sensors', 'displaycond')) {
            Schema::table('sensors', function (Blueprint $table) {
                $table->dropColumn('displaycond');
            });
        }
        if (Schema::hasColumn('sensors', 'show')) {
            Schema::table('sensors', function (Blueprint $table) {
                $table->dropColumn('show');
            });
        }
    }
}

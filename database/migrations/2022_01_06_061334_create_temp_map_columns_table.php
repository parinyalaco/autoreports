<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempMapColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_map_columns', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('displayname');
            $table->string('mapcolumn');
            $table->string('typecolumn');
            $table->string('conditions')->nullable();
            $table->string('status',50)->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_map_columns');
    }
}

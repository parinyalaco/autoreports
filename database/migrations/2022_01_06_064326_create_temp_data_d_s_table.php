<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempDataDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_data_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('temp_data_m_id');
            $table->integer('temp_map_column_id');
            $table->datetime('temp_datetime');
            $table->float('temp_val');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_data_d_s');
    }
}

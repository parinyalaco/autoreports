<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update2022050902 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'sensors',
            function (Blueprint $table) {
                $table->string('displayname',100)->nullable();
                $table->string('coil', 100)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sensors', 'displayname')) {
            Schema::table('sensors', function (Blueprint $table) {
                $table->dropColumn('displayname');
            });
        }
        if (Schema::hasColumn('sensors', 'coil')) {
            Schema::table('sensors', function (Blueprint $table) {
                $table->dropColumn('coil');
            });
        }
    }
}

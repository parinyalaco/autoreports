<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220509 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'flag_act_times',
            function (Blueprint $table) {
                $table->integer('sensor_id')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('flag_act_times', 'sensor_id')) {
            Schema::table('flag_act_times', function (Blueprint $table) {
                $table->dropColumn('sensor_id');
            });
        }
    }
}

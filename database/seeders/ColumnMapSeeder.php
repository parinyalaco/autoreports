<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class ColumnMapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('temp_map_columns')->insert([
            'name' => 'Anti Cold/EVT 1/Coil 1',
            'displayname' => 'Anti Cold room 4',
            'mapcolumn' => 'EVT 1',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Anti Cold room 4/EVT 2/Coil 1',
            'displayname' => 'Air lock Cold room 4',
            'mapcolumn' => 'EVT 2',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Anti Cold room 4/EVT 3/Coil 1',
            'displayname' => 'Air lock Cold room 4',
            'mapcolumn' => 'EVT 3',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 4/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 4',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 5/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 5',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 6/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 6',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 7/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 7',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 8/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 8',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 9/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 9',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 10/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 10',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 4/EVT 11/Coil 1',
            'displayname' => 'Pent house cold room 4',
            'mapcolumn' => 'EVT 11',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 1/EVT 12/Coil 1',
            'displayname' => 'Pent house cold room 1',
            'mapcolumn' => 'EVT 12',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 1/EVT 13/Coil 1',
            'displayname' => 'Pent house cold room 1',
            'mapcolumn' => 'EVT 13',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 1/EVT 14/Coil 1',
            'displayname' => 'Pent house cold room 1',
            'mapcolumn' => 'EVT 14',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 1/EVT 15/Coil 1',
            'displayname' => 'Pent house cold room 1',
            'mapcolumn' => 'EVT 15',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 1/EVT 16/Coil 1',
            'displayname' => 'Pent house cold room 1',
            'mapcolumn' => 'EVT 16',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert([
            'name' => 'Pent house cold room 1/EVT 17/Coil 1',
            'displayname' => 'Pent house cold room 1',
            'mapcolumn' => 'EVT 17',
            'typecolumn' => 'Coil 1',
            'conditions' => '',
        ]);

        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold/EVT 18/Coil 1', 'displayname' => 'Anti Cold', 'mapcolumn' => 'EVT 18', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold/EVT 19/Coil 1', 'displayname' => 'Anti Cold', 'mapcolumn' => 'EVT 19', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Air Lock 1/EVT 20/Coil 1', 'displayname' => 'Air Lock 1', 'mapcolumn' => 'EVT 20', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 4/EVT 21/Coil 1', 'displayname' => 'Pent house cold room 4', 'mapcolumn' => 'EVT 21', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Air Lock 3/EVT 22/Coil 1', 'displayname' => 'Air Lock 3', 'mapcolumn' => 'EVT 22', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'LOADING/EVT 23/Coil 1', 'displayname' => 'LOADING', 'mapcolumn' => 'EVT 23', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'ANTE PACK/EVT 24/Coil 2', 'displayname' => 'ANTE PACK', 'mapcolumn' => 'EVT 24', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'ANTE 1/4/EVT 25/Coil 3', 'displayname' => 'ANTE 1/4', 'mapcolumn' => 'EVT 25', 'typecolumn' => 'Coil 3', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'ANTE 2/4/EVT 26/Coil 4', 'displayname' => 'ANTE 2/4', 'mapcolumn' => 'EVT 26', 'typecolumn' => 'Coil 4', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'ANTE 3/4/EVT 27/Coil 5', 'displayname' => 'ANTE 3/4', 'mapcolumn' => 'EVT 27', 'typecolumn' => 'Coil 5', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'ANTE 4/4/EVT 28/Coil 6', 'displayname' => 'ANTE 4/4', 'mapcolumn' => 'EVT 28', 'typecolumn' => 'Coil 6', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'AIR LOCK PF/EVT 29/Coil 1', 'displayname' => 'AIR LOCK PF', 'mapcolumn' => 'EVT 29', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'ANTE PF/EVT 30/Coil 1', 'displayname' => 'ANTE PF', 'mapcolumn' => 'EVT 30', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'After Blanch Zone/EVT 31/Coil 1', 'displayname' => 'After Blanch Zone', 'mapcolumn' => 'EVT 31', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'After Blanch Zone/EVT 32/Coil 1', 'displayname' => 'After Blanch Zone', 'mapcolumn' => 'EVT 32', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'After Blanch Zone/EVT 33/Coil 1', 'displayname' => 'After Blanch Zone', 'mapcolumn' => 'EVT 33', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'After Blanch Zone/EVT 34/Coil 1', 'displayname' => 'After Blanch Zone', 'mapcolumn' => 'EVT 34', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 35/Coil 1', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 35', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 36/Coil 2', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 36', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 37/Coil 3', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 37', 'typecolumn' => 'Coil 3', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 38/Coil 4', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 38', 'typecolumn' => 'Coil 4', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 39/Coil 5', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 39', 'typecolumn' => 'Coil 5', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 40/Coil 6', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 40', 'typecolumn' => 'Coil 6', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 41/Coil 7', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 41', 'typecolumn' => 'Coil 7', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 4/EVT 42/Coil 8', 'displayname' => 'Anti Cold room 4', 'mapcolumn' => 'EVT 42', 'typecolumn' => 'Coil 8', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Anti Cold room 2/EVT 43/Coil 1', 'displayname' => 'Anti Cold room 2', 'mapcolumn' => 'EVT 43', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 44/Coil 1', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 44', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 45/Coil 2', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 45', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 46/Coil 3', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 46', 'typecolumn' => 'Coil 3', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 47/Coil 4', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 47', 'typecolumn' => 'Coil 4', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 48/Coil 5', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 48', 'typecolumn' => 'Coil 5', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 49/Coil 6', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 49', 'typecolumn' => 'Coil 6', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 50/Coil 7', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 50', 'typecolumn' => 'Coil 7', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Pent house cold room 3/EVT 51/Coil 8', 'displayname' => 'Pent house cold room 3', 'mapcolumn' => 'EVT 51', 'typecolumn' => 'Coil 8', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(

            ['name' => 'Manual pack/GAU1/Coil 1', 'displayname' => 'Manual pack', 'mapcolumn' => 'GAU1', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Manual pack/GAU2/Coil 2', 'displayname' => 'Manual pack', 'mapcolumn' => 'GAU2', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'X-ray/GAU3/Coil 1', 'displayname' => 'X-ray', 'mapcolumn' => 'GAU3', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'X-ray/GAU4/Coil 2', 'displayname' => 'X-ray', 'mapcolumn' => 'GAU4', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Auto pack/GAU5/Coil 1', 'displayname' => 'Auto pack', 'mapcolumn' => 'GAU5', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Auto pack/GAU6/Coil 2', 'displayname' => 'Auto pack', 'mapcolumn' => 'GAU6', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'LINE PK/GAU7/Coil 1', 'displayname' => 'LINE PK', 'mapcolumn' => 'GAU7', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'LINE PK/GAU8/Coil 2', 'displayname' => 'LINE PK', 'mapcolumn' => 'GAU8', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'LINE PK/GAU9/Coil 3', 'displayname' => 'LINE PK', 'mapcolumn' => 'GAU9', 'typecolumn' => 'Coil 3', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'LINE PK/GAU10/Coil 4', 'displayname' => 'LINE PK', 'mapcolumn' => 'GAU10', 'typecolumn' => 'Coil 4', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Cartoning/GAU11/Coil 1', 'displayname' => 'Cartoning', 'mapcolumn' => 'GAU11', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Choco PK/GAU12/Coil 1', 'displayname' => 'Choco PK', 'mapcolumn' => 'GAU12', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Choco PK/GAU13/Coil 2', 'displayname' => 'Choco PK', 'mapcolumn' => 'GAU13', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Coating Room/GAU14/Coil 1', 'displayname' => 'Coating Room', 'mapcolumn' => 'GAU14', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Coating Room/GAU15/Coil 2', 'displayname' => 'Coating Room', 'mapcolumn' => 'GAU15', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Carry Room/GAU16/Coil 1', 'displayname' => 'Carry Room', 'mapcolumn' => 'GAU16', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Cutting Room/GAU17/Coil 1', 'displayname' => 'Cutting Room', 'mapcolumn' => 'GAU17', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Cutting Room/GAU18/Coil 2', 'displayname' => 'Cutting Room', 'mapcolumn' => 'GAU18', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Cutting Room/GAU19/Coil 3', 'displayname' => 'Cutting Room', 'mapcolumn' => 'GAU19', 'typecolumn' => 'Coil 3', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Washing Room/GAU20/Coil 1', 'displayname' => 'Washing Room', 'mapcolumn' => 'GAU20', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Coolling room/GAU21/Coil 1', 'displayname' => 'Coolling room', 'mapcolumn' => 'GAU21', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Coolling room/GAU22/Coil 2', 'displayname' => 'Coolling room', 'mapcolumn' => 'GAU22', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Filling Room/GAU23/Coil 1', 'displayname' => 'Filling Room', 'mapcolumn' => 'GAU23', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Filling Room/GAU24/Coil 2', 'displayname' => 'Filling Room', 'mapcolumn' => 'GAU24', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'PK Room F3/GAU25/Coil1', 'displayname' => 'PK Room F3', 'mapcolumn' => 'GAU25', 'typecolumn' => 'Coil1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'PK Room IQF1-2/GAU26/Coil 1', 'displayname' => 'PK Room IQF1-2', 'mapcolumn' => 'GAU26', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'PK Room IQF1-2/GAU27/Coil 2', 'displayname' => 'PK Room IQF1-2', 'mapcolumn' => 'GAU27', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'PF PK/GAU28/Coil 1', 'displayname' => 'PF PK', 'mapcolumn' => 'GAU28', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Rawmat Room/GAU29/Coil 1', 'displayname' => 'Rawmat Room', 'mapcolumn' => 'GAU29', 'typecolumn' => 'Coil 1', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Rawmat Room/GAU30/Coil 2', 'displayname' => 'Rawmat Room', 'mapcolumn' => 'GAU30', 'typecolumn' => 'Coil 2', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Rawmat Room/GAU31/Coil 3', 'displayname' => 'Rawmat Room', 'mapcolumn' => 'GAU31', 'typecolumn' => 'Coil 3', 'conditions' => '',]
        );
        DB::table('temp_map_columns')->insert(
            ['name' => 'Rawmat Room/GAU32/Coil 4', 'displayname' => 'Rawmat Room', 'mapcolumn' => 'GAU32', 'typecolumn' => 'Coil 4', 'conditions' => '',]
        );
    }
}

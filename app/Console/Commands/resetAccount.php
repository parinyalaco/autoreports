<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class resetAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:reset {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command reset password account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $username = $this->argument('username');
        $user = \App\Models\User::where('email', $username)->first();
        $user->setPasswordAttribute('12345678');
        $user->update();
        echo "User '".$username."' has reset !!!";
        return 0;
    }
}

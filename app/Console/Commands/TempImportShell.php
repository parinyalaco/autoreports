<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use SimpleXLSX;
use App\Models\TempMapColumn;
use App\Models\TempDataM;
use App\Models\TempDataD;

class TempImportShell extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tempdata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Temperature from SCADA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $directory = config('myconfig.directory.tempscada.queues');
        $completedirectory = config('myconfig.directory.tempscada.completes');
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        $columnmaplists = TempMapColumn::where('status', 'Active')->pluck('id', 'mapcolumn');

        foreach ($scanned_directory as $fileupload) {
            $datafilename = explode('_', $fileupload);
            $datetxt = explode('.', $datafilename[3]);
            $newdatatxt = $datetxt[0][4] . $datetxt[0][5] . $datetxt[0][6] . $datetxt[0][7] . "-" . $datetxt[0][2] . $datetxt[0][3] . "-" . $datetxt[0][0] . $datetxt[0][1];
            $datedata = date('Y-m-d', strtotime($newdatatxt));
            $this->_readdata( $directory."/" .$fileupload, $datedata, $columnmaplists);
            echo "Import File: ". $fileupload;
            if (copy($directory . '/' . $fileupload, $completedirectory . '/' . $fileupload)) {
                unlink($directory . '/' . $fileupload);
                echo " Complete!;\n" ;
            }
        }
    }

    private function _readdata($filename,$tempdate, $columnmaplists){
        $columnTime = 0;
        if ($xlsx = SimpleXLSX::parse($filename)) {
            $sheetlists = $xlsx->sheetNames();
            foreach ($sheetlists as $key => $value) {
                $columnmapsheet = array();
                $tmpMaster = array();
                $tmpMaster['filename'] = $filename;
                $tmpMaster['tempdate'] = $tempdate;
                $tmpMaster['result'] = "";
                $tmpMaster['status'] ='Active';
                $tmpMaster['sheet'] = $value;
                $tmpid = 0;
                $tmpid = TempDataM::create($tmpMaster)->id;
                foreach ($xlsx->rows($key) as $r => $row) {
                    if($r == 6){
                        foreach ($row as $cr => $crow) {
                            if(isset($columnmaplists[$crow])){
                                 $columnmapsheet[$cr] = $columnmaplists[$crow];
                            }
                        }
                    }else{
                        if($r > 7){
                            $tmpData = array();
                            foreach ($row as $cr => $crow) {
                                if($cr == 0 && strlen($crow) < 6){
                                    $datetimeDATA = \Carbon\Carbon::parse($tempdate." ".$crow)->format('Y-m-d H:i');
                                    $tmpData['temp_datetime'] = $datetimeDATA;
                                }
                                if(isset($columnmapsheet[$cr]) && !empty($crow) ){
                                    $tmpData['temp_map_column_id'] = $columnmapsheet[$cr];
                                    $tmpData['temp_val'] = $crow;
                                    $tmpData['temp_data_m_id'] = $tmpid;

                                    if (!empty($tmpData) && !empty($tmpData['temp_val'])) {
                                        $tempdata = TempDataD::where('temp_datetime', $tmpData['temp_datetime'])
                                        ->where('temp_map_column_id', $columnmapsheet[$cr])
                                        ->first();
                                        if (!empty($tempdata)) {
                                            $tempdata->temp_val = $crow;
                                            $tempdata->temp_data_m_id = $tmpid;
                                            $tempdata->update();
                                        } else {
                                            TempDataD::create($tmpData);
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }  
            }
        }
    }
}

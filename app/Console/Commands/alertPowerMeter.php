<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class alertPowerMeter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:checkConnPowerMeter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sqlTxt = "select A.metername ,A.location, A.P,A.Type,A.KWH,A.Tarrif,A.TarUnit,count(A.id) from ";
        $sqlTxt .= "(SELECT datatb.id,datatb.RecTime,metertb.metername ,metertb.location, datatb.P,datatb.Type,datatb.KWH,datatb.Tarrif,datatb.TarUnit ";
        $sqlTxt .= "FROM lr_042023 as datatb left join metertbl as metertb ";
        $sqlTxt .= "on  metertb.metername = datatb.metername and metertb.meteradd = metertb.meteradd ";
        $sqlTxt .= "where datatb.RecTime > DATE_SUB(NOW(), INTERVAL '80' minute) ) as A ";
        $sqlTxt .= "group by A.metername ,A.location, A.P,A.Type,A.KWH,A.Tarrif,A.TarUnit having count(A.id) >= 5 ";
        $sqlTxt .= "order by A.metername ,A.location";

        $dbPowerMeterObj = DB::connection('powermeter');
        $data = $dbPowerMeterObj->select(DB::raw($sqlTxt));

        if (!empty($data)) {
            $errotPart = "";
            foreach ($data as $errObj) {
                $errotPart .= $errObj->location.",\n";
            }
            $errotPart .= "Lost conenction!!";
            $this->line_notify($errotPart);
        }
    }

    function line_notify($message, $Token = 'tPqjl1wGe3INmtBodoMv9KtqGa523rkex3Aa7Ijzx0b')
    {
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = array('Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . '',);
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        $result = "";
        //Check error
        if (curl_error($chOne)) {
            $result = 'error:' . curl_error($chOne);
        } else {
            //  $result_ = json_decode($result, true);
            //    $result = "status : " . $result_['status'];
            //   $result .= "message : " . $result_['message'];
        }
        curl_close($chOne);
        return $result;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TempMapColumn;
use App\Models\TempDataM;
use App\Models\FlagActTime;
use App\Models\Sensor;
use App\Models\SensorData;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

use App\Mail\TempScadaRteEmail;
use App\Mail\TempScadaEmail;
use App\Mail\TempScadaAllEmail;
use Illuminate\Support\Facades\Mail;

class GenReportsShell extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gen:report {reportname}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Report for Emails Separate by Type Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reportname = $this->argument('reportname');
        $date = date('Y-m-d', strtotime("-1 days"));;
        if ($reportname == 'tempQcScada') {
            $filename = $this->_tempQcScada($date);
        } elseif ($reportname == 'tempQcRTEScada') {
            $filename = $this->_tempQcRTEScada($date);
        } elseif ($reportname == 'all') {
            $filename = $this->_tempQcAllScada($date);
        } else {
            echo "No Report:" . $reportname;
        }
        return 0;
    }

    private function _tempQcScada($date)
    {
        //echo  "tempQcScada:" . $date;
        $allColumnsDatas = TempMapColumn::where('status', 'Active')->where('show', 'Yes')->orderBy('seq')->get();
        $allFlagDatas = FlagActTime::where('status', 'Active')->where('temp_map_column_id', '>', 0)->get();
        //dd($allColumnsData);
        $timeList = array();
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $timeList[] = "0" . $i . ":00";
            } else {
                $timeList[] = $i . ":00";
            }
        }
        //foreach ($allColumnsDatas as $ColumnsData) {
        // echo $ColumnsData->displayname." ". $ColumnsData->mapcolumn." ".$ColumnsData->conditions."\n";
        //}
        $flagData = array();
        foreach ($allFlagDatas as $flagDataObj) {
            $flagData[$flagDataObj->temp_map_column_id][$date . " " . $flagDataObj->settime] = $flagDataObj->flag;
        }

        $rawData = TempDataM::where('tempdate', $date)->get();
        //dd($rawData);
        if ($rawData->count() > 0) {
            $data = array();
            foreach ($rawData as $masterdata) {
                foreach ($masterdata->tempdatads as $tempdatad) {
                    $data[$tempdatad->temp_map_column_id][date("Y-m-d H:i", strtotime($tempdatad->temp_datetime))] = $tempdatad->temp_val;
                }
            }
            //dd($data);
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValueByColumnAndRow(1, 1, "รายงานอุณหภูมิห้อง วันที่ " . $date);

            $sheet->setCellValueByColumnAndRow(1, 3, "รายละเอียด");
            $sheet->setCellValueByColumnAndRow(2, 3, "Coil");
            $sheet->setCellValueByColumnAndRow(3, 3, " Temp SPEC (˚C)");
            foreach ($timeList as $key => $value) {
                $sheet->setCellValueByColumnAndRow(4 + $key, 3, $value);
            }

            $rowdata = 4;
            $loopdata = 0;
            foreach ($allColumnsDatas as $ColumnsData) {
                $sheet->setCellValueByColumnAndRow(1, $rowdata + $loopdata, $ColumnsData->displayname);
                $sheet->setCellValueByColumnAndRow(2, $rowdata + $loopdata, $ColumnsData->mapcolumn);
                $sheet->setCellValueByColumnAndRow(3, $rowdata + $loopdata, $ColumnsData->displaycond);
                $codeloc = "";
                foreach ($timeList as $key => $value) {
                    if (isset($data[$ColumnsData->id]) && isset($data[$ColumnsData->id][$date . " " . $value])) {

                        if (isset($flagData[$ColumnsData->id][$date . " " . $value])) {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, number_format($data[$ColumnsData->id][$date . " " . $value], 2, '.', '') . "," . $flagData[$ColumnsData->id][$date . " " . $value]);
                        } else {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $data[$ColumnsData->id][$date . " " . $value]);
                        }

                        $codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();

                        if (!empty($ColumnsData->conditions2)) {
                            if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                //echo $dat a[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                $pos = strpos($ColumnsData->conditions2, '<');
                                if ($pos === false) {
                                    $condexplo = explode("ถึง", $ColumnsData->conditions2);
                                    $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                }
                                if (eval($strcond)) {
                                    //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                    //echo $codeloc;
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFFFFF00');
                                } else {
                                    if (!empty($ColumnsData->conditions)) {

                                        if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                            //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                            $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                            $pos = strpos($ColumnsData->conditions, '<');
                                            if ($pos === false) {
                                                $condexplo = explode("ถึง", $ColumnsData->conditions);
                                                $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                            }
                                            if (!eval($strcond)) {
                                                //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                                //echo $codeloc;
                                                $sheet->getStyle($codeloc)
                                                    ->getFill()
                                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                    ->getStartColor()->setARGB('FFFF0000');
                                            }
                                        } else {
                                            $sheet->getStyle($codeloc)
                                                ->getFill()
                                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                ->getStartColor()->setARGB('FF000000');
                                        }
                                    }
                                }
                            } else {
                                $sheet->getStyle($codeloc)
                                    ->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setARGB('FF000000');
                            }
                        } else {
                            if (!empty($ColumnsData->conditions)) {

                                if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                    //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                    $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                    $pos = strpos($ColumnsData->conditions, '<');
                                    if ($pos === false) {
                                        $condexplo = explode("ถึง", $ColumnsData->conditions);
                                        $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                    }
                                    if (!eval($strcond)) {
                                        //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                        //echo $codeloc;
                                        $sheet->getStyle($codeloc)
                                            ->getFill()
                                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                            ->getStartColor()->setARGB('FFFF0000');
                                    }
                                } else {
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FF000000');
                                }
                            }
                        }
                    } else {
                        if (isset($flagData[$ColumnsData->id][$date . " " . $value])) {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $flagData[$ColumnsData->id][$date . " " . $value]);
                        }
                    }
                }
                $loopdata++;
            }
            $sheet->getStyle('A3:' . $codeloc)
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $writer = new Xlsx($spreadsheet);

            $filename = "exportdata-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            $allStaff = config('myconfig.emaillist');
            $ccStaff = config('myconfig.emailcclist');

            $dataset = array();

            $dataset['allColumnsDatas'] = $allColumnsDatas;
            $dataset['timeList'] = $timeList;
            $dataset['data'] = $data;
            $dataset['flagData'] = $flagData;


            //Mail::to(['PKP' => 'parinya.k@lannaagro.com'])
            Mail::to($allStaff)
                ->cc($ccStaff)
                ->send(new TempScadaEmail($filename, $date, $dataset));

            return $filename;
        }
    }

    private function _tempQcRTEScada($date)
    {
        //echo  "tempQcScada:" . $date;
        $allColumnsDatas = Sensor::where('status', 'Active')->where('show', 'Yes')->orderBy('seq')->get();
        $allFlagDatas = FlagActTime::where('status', 'Active')->where('sensor_id', '>',0)->get();
        //dd($allColumnsData);
        $timeList = array();
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $timeList[] = "0" . $i . ":00";
            } else {
                $timeList[] = $i . ":00";
            }
        }
        //foreach ($allColumnsDatas as $ColumnsData) {
        // echo $ColumnsData->displayname." ". $ColumnsData->mapcolumn." ".$ColumnsData->conditions."\n";
        //}
        $flagData = array();
        foreach ($allFlagDatas as $flagDataObj) {
            $flagData[$flagDataObj->temp_map_column_id][$date . " " . $flagDataObj->settime] = $flagDataObj->flag;
        }

        $rawData = SensorData::where('temp_datetime','>=', $date." 00:00")->where('temp_datetime', '<=', $date . " 23:59")->get();
        // dd($rawData);
        if ($rawData->count() > 0) {
            $data = array();
            foreach ($rawData as $tempdatad) {
                //foreach ($masterdata->tempdatads as $tempdatad) {
                $data[$tempdatad->sensor_id][date("Y-m-d H:i", strtotime($tempdatad->temp_datetime))] = $tempdatad->value;
                //}
            }
            // dd($data);
             $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValueByColumnAndRow(1, 1, "รายงานอุณหภูมิห้อง วันที่ " . $date);

            $sheet->setCellValueByColumnAndRow(1, 3, "รายละเอียด");
            $sheet->setCellValueByColumnAndRow(2, 3, "Coil");
            $sheet->setCellValueByColumnAndRow(3, 3, " Temp SPEC (˚C)");
            foreach ($timeList as $key => $value) {
                $sheet->setCellValueByColumnAndRow(4 + $key, 3, $value);
            }

            $rowdata = 4;
            $loopdata = 0;
            foreach ($allColumnsDatas as $ColumnsData) {
                $sheet->setCellValueByColumnAndRow(1, $rowdata + $loopdata, $ColumnsData->displayname);
                // $coils = explode(' ', $ColumnsData->name);
                $sheet->setCellValueByColumnAndRow(2, $rowdata + $loopdata, $ColumnsData->coil);
                $sheet->setCellValueByColumnAndRow(3, $rowdata + $loopdata, $ColumnsData->displaycond);
                $codeloc = "";
                foreach ($timeList as $key => $value) {
                    if (isset($data[$ColumnsData->id]) && isset($data[$ColumnsData->id][$date . " " . $value])) {

                        if (isset($flagData[$ColumnsData->id][$date . " " . $value])) {
                            // $sheet->
                            // setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $data[$ColumnsData->id][$date . " " . $value]);
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, number_format($data[$ColumnsData->id][$date . " " . $value], 2, '.', '') . "," . $flagData[$ColumnsData->id][$date . " " . $value]);
                        } else {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $data[$ColumnsData->id][$date . " " . $value]);
                        }

                        $codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();

                        if (!empty($ColumnsData->conditions2)) {
                            if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                //echo $dat a[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                $pos = strpos($ColumnsData->conditions2, '<');
                                if ($pos === false) {
                                    $condexplo = explode("ถึง", $ColumnsData->conditions2);
                                    $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                }
                                //  echo ($strcond);
                                if (eval($strcond)) {
                                    //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                    //echo $codeloc;
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFFFFF00');
                                } else {
                                    if (!empty($ColumnsData->conditions)) {

                                        if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                            //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                            $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                            $pos = strpos($ColumnsData->conditions, '<');
                                            if ($pos === false) {
                                                $condexplo = explode("ถึง", $ColumnsData->conditions);
                                                $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                            }
                                            // echo ($strcond);
                                            if (!eval($strcond)) {
                                                //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                                //echo $codeloc;
                                                $sheet->getStyle($codeloc)
                                                    ->getFill()
                                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                    ->getStartColor()->setARGB('FFFF0000');
                                            }
                                        } else {
                                            $sheet->getStyle($codeloc)
                                                ->getFill()
                                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                ->getStartColor()->setARGB('FF000000');
                                        }
                                    }
                                }
                            } else {
                                $sheet->getStyle($codeloc)
                                    ->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setARGB('FF000000');
                            }
                        } else {
                            if (!empty($ColumnsData->conditions)) {

                                if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                    //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                    $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                    $pos = strpos($ColumnsData->conditions, '<');
                                    if ($pos === false) {
                                        $condexplo = explode("ถึง", $ColumnsData->conditions);
                                        $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                    }
                                    if (!eval($strcond)) {
                                        //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                        //echo $codeloc;
                                        $sheet->getStyle($codeloc)
                                            ->getFill()
                                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                            ->getStartColor()->setARGB('FFFF0000');
                                    }
                                } else {
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FF000000');
                                }
                            }
                        }
                    } else {
                        if (isset($flagData[$ColumnsData->id][$date . " " . $value])) {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $flagData[$ColumnsData->id][$date . " " . $value]);
                        }
                    }
                }
                $loopdata++;
            }
            $sheet->getStyle('A3:' . $codeloc)
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $writer = new Xlsx($spreadsheet);

            $filename = "exportdatarte-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            $allStaff = config('myconfig.emaillist');
            $ccStaff = config('myconfig.emailcclist');

            $dataset = array();

            $dataset['allColumnsDatas'] = $allColumnsDatas;
            $dataset['timeList'] = $timeList;
            $dataset['data'] = $data;
            $dataset['flagData'] = $flagData;


            Mail::to(['PKP' => 'parinya.k@lannaagro.com'])
            // Mail::to($allStaff)
            //     ->cc($ccStaff)
                ->send(new TempScadaRteEmail($filename, $date, $dataset));

            return $filename;
        }
    }

    private function
    _tempQcAllScada($date)
    {
        //echo  "tempQcScada:" . $date;
        $allColumnsDatas = TempMapColumn::where('status', 'Active')->where('show', 'Yes')->orderBy('seq')->get();
        $allFlagDatas = FlagActTime::where('status', 'Active')->where('temp_map_column_id', '>', 0)->get();
        //dd($allColumnsData);
        $timeList = array();
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $timeList[] = "0" . $i . ":00";
            } else {
                $timeList[] = $i . ":00";
            }
        }
        //foreach ($allColumnsDatas as $ColumnsData) {
        // echo $ColumnsData->displayname." ". $ColumnsData->mapcolumn." ".$ColumnsData->conditions."\n";
        //}
        $flagData = array();
        foreach ($allFlagDatas as $flagDataObj) {
            $flagData[$flagDataObj->temp_map_column_id][$date . " " . $flagDataObj->settime] = $flagDataObj->flag;
        }

        $rawData = TempDataM::where('tempdate', $date)->get();
        //dd($rawData);
        if ($rawData->count() > 0) {
            $data = array();
            foreach ($rawData as $masterdata) {
                foreach ($masterdata->tempdatads as $tempdatad) {
                    $data[$tempdatad->temp_map_column_id][date("Y-m-d H:i", strtotime($tempdatad->temp_datetime))] = $tempdatad->temp_val;
                }
            }
            //dd($data);
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValueByColumnAndRow(1, 1, "รายงานอุณหภูมิห้อง วันที่ " . $date);

            $sheet->setCellValueByColumnAndRow(1, 3, "รายละเอียด");
            $sheet->setCellValueByColumnAndRow(2, 3, "Coil");
            $sheet->setCellValueByColumnAndRow(3, 3, " Temp SPEC (˚C)");
            foreach ($timeList as $key => $value) {
                $sheet->setCellValueByColumnAndRow(4 + $key, 3, $value);
            }

            $rowdata = 4;
            $loopdata = 0;
            foreach ($allColumnsDatas as $ColumnsData) {
                $sheet->setCellValueByColumnAndRow(1, $rowdata + $loopdata, $ColumnsData->displayname);
                $sheet->setCellValueByColumnAndRow(2, $rowdata + $loopdata, $ColumnsData->mapcolumn);
                $sheet->setCellValueByColumnAndRow(3, $rowdata + $loopdata, $ColumnsData->displaycond);
                $codeloc = "";
                foreach ($timeList as $key => $value) {
                    if (isset($data[$ColumnsData->id]) && isset($data[$ColumnsData->id][$date . " " . $value])) {

                        if (isset($flagData[$ColumnsData->id][$date . " " . $value])) {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, number_format($data[$ColumnsData->id][$date . " " . $value], 2, '.', '') . "," . $flagData[$ColumnsData->id][$date . " " . $value]);
                        } else {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $data[$ColumnsData->id][$date . " " . $value]);
                        }

                        $codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();

                        if (!empty($ColumnsData->conditions2)) {
                            if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                //echo $dat a[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                $pos = strpos($ColumnsData->conditions2, '<');
                                if ($pos === false) {
                                    $condexplo = explode("ถึง", $ColumnsData->conditions2);
                                    $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                }
                                if (eval($strcond)) {
                                    //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                    //echo $codeloc;
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFFFFF00');
                                } else {
                                    if (!empty($ColumnsData->conditions)) {

                                        if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                            //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                            $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                            $pos = strpos($ColumnsData->conditions, '<');
                                            if ($pos === false) {
                                                $condexplo = explode("ถึง", $ColumnsData->conditions);
                                                $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                            }
                                            if (!eval($strcond)) {
                                                //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                                //echo $codeloc;
                                                $sheet->getStyle($codeloc)
                                                    ->getFill()
                                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                    ->getStartColor()->setARGB('FFFF0000');
                                            }
                                        } else {
                                            $sheet->getStyle($codeloc)
                                                ->getFill()
                                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                ->getStartColor()->setARGB('FF000000');
                                        }
                                    }
                                }
                            } else {
                                $sheet->getStyle($codeloc)
                                    ->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setARGB('FF000000');
                            }
                        } else {
                            if (!empty($ColumnsData->conditions)) {

                                if (!empty($data[$ColumnsData->id][$date . " " . $value])) {
                                    //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                    $strcond = "return " . $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                    $pos = strpos($ColumnsData->conditions, '<');
                                    if ($pos === false) {
                                        $condexplo = explode("ถึง", $ColumnsData->conditions);
                                        $strcond = "return (" . $data[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                    }
                                    if (!eval($strcond)) {
                                        //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                        //echo $codeloc;
                                        $sheet->getStyle($codeloc)
                                            ->getFill()
                                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                            ->getStartColor()->setARGB('FFFF0000');
                                    }
                                } else {
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FF000000');
                                }
                            }
                        }
                    } else {
                        if (isset($flagData[$ColumnsData->id][$date . " " . $value])) {
                            $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $flagData[$ColumnsData->id][$date . " " . $value]);
                        }
                    }
                }
                $loopdata++;
            }




            $allColumnsRteDatas = Sensor::where('status', 'Active')->where('show', 'Yes')->orderBy('seq')->get();
            $allFlagRteDatas = FlagActTime::where('status', 'Active')->where('sensor_id', '>', 0)->get();

            $flagRteData = array();
            foreach ($allFlagRteDatas as $flagDataObj) {
                $flagRteData[$flagDataObj->sensor_id][$date . " " . $flagDataObj->settime] = $flagDataObj->flag;
            }

            $rawRteData = SensorData::where('temp_datetime', '>=', $date . " 00:00")->where('temp_datetime', '<=', $date . " 23:59")->get();
            // dd($rawData);
            $dataRte = array();
            if ($rawRteData->count() > 0) {

                foreach ($rawRteData as $tempdatad) {
                    //foreach ($masterdata->tempdatads as $tempdatad) {
                    $dataRte[$tempdatad->sensor_id][date("Y-m-d H:i", strtotime($tempdatad->temp_datetime))] = $tempdatad->value;
                    //}
                }

                $rowdata = 4;
               // $loopdata = 0;
                foreach ($allColumnsRteDatas as $ColumnsData) {
                    $sheet->setCellValueByColumnAndRow(1, $rowdata + $loopdata, $ColumnsData->displayname);
                    // $coils = explode(' ', $ColumnsData->name);
                    $sheet->setCellValueByColumnAndRow(2, $rowdata + $loopdata, $ColumnsData->coil);
                    $sheet->setCellValueByColumnAndRow(3, $rowdata + $loopdata, $ColumnsData->displaycond);
                    $codeloc = "";
                    foreach ($timeList as $key => $value) {
                        if (isset($dataRte[$ColumnsData->id]) && isset($dataRte[$ColumnsData->id][$date . " " . $value])) {

                            if (isset($flagRteData[$ColumnsData->id][$date . " " . $value])) {
                                // $sheet->
                                // setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $data[$ColumnsData->id][$date . " " . $value]);
                                $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, number_format($dataRte[$ColumnsData->id][$date . " " . $value], 2, '.', '') . "," . $flagRteData[$ColumnsData->id][$date . " " . $value]);
                            } else {
                                $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $dataRte[$ColumnsData->id][$date . " " . $value]);
                            }

                            $codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();

                            if (!empty($ColumnsData->conditions2)) {
                                if (!empty($dataRte[$ColumnsData->id][$date . " " . $value])) {
                                    //echo $dat a[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                    $strcond = "return " . $dataRte[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                    $pos = strpos($ColumnsData->conditions2, '<');
                                    if ($pos === false) {
                                        $condexplo = explode("ถึง", $ColumnsData->conditions2);
                                        $strcond = "return (" . $dataRte[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $dataRte[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                    }
                                    //  echo ($strcond);
                                    if (eval($strcond)) {
                                        //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                        //echo $codeloc;
                                        $sheet->getStyle($codeloc)
                                            ->getFill()
                                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                            ->getStartColor()->setARGB('FFFFFF00');
                                    } else {
                                        if (!empty($ColumnsData->conditions)) {

                                            if (!empty($dataRte[$ColumnsData->id][$date . " " . $value])) {
                                                //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                                $strcond = "return " . $dataRte[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                                $pos = strpos($ColumnsData->conditions, '<');
                                                if ($pos === false) {
                                                    $condexplo = explode("ถึง", $ColumnsData->conditions);
                                                    $strcond = "return (" . $dataRte[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $dataRte[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                                }
                                                // echo ($strcond);
                                                if (!eval($strcond)) {
                                                    //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                                    //echo $codeloc;
                                                    $sheet->getStyle($codeloc)
                                                        ->getFill()
                                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                        ->getStartColor()->setARGB('FFFF0000');
                                                }
                                            } else {
                                                $sheet->getStyle($codeloc)
                                                    ->getFill()
                                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                    ->getStartColor()->setARGB('FF000000');
                                            }
                                        }
                                    }
                                } else {
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FF000000');
                                }
                            } else {
                                if (!empty($ColumnsData->conditions)) {

                                    if (!empty($dataRte[$ColumnsData->id][$date . " " . $value])) {
                                        //echo $data[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions;
                                        $strcond = "return " . $dataRte[$ColumnsData->id][$date . " " . $value] . $ColumnsData->conditions . ";";
                                        $pos = strpos($ColumnsData->conditions, '<');
                                        if ($pos === false) {
                                            $condexplo = explode("ถึง", $ColumnsData->conditions);
                                            $strcond = "return (" . $dataRte[$ColumnsData->id][$date . " " . $value] . ">=" . $condexplo[0] . " && " . $dataRte[$ColumnsData->id][$date . " " . $value] . "<=" . $condexplo[1] . ");";
                                        }
                                        if (!eval($strcond)) {
                                            //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                            //echo $codeloc;
                                            $sheet->getStyle($codeloc)
                                                ->getFill()
                                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                ->getStartColor()->setARGB('FFFF0000');
                                        }
                                    } else {
                                        $sheet->getStyle($codeloc)
                                            ->getFill()
                                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                            ->getStartColor()->setARGB('FF000000');
                                    }
                                }
                            }
                        } else {
                            if (isset($flagRteData[$ColumnsData->id][$date . " " . $value])) {
                                $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $flagRteData[$ColumnsData->id][$date . " " . $value]);
                            }
                        }
                    }
                    $loopdata++;
                }
            }

            $sheet->getStyle('A3:' . $codeloc)
            ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $writer = new Xlsx($spreadsheet);

            $filename = "exportdata-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            $allStaff = config('myconfig.emaillist');
            $ccStaff = config('myconfig.emailcclist');

            $dataset = array();

            $dataset['allColumnsDatas'] = $allColumnsDatas;
            $dataset['timeList'] = $timeList;
            $dataset['data'] = $data;
            $dataset['flagData'] = $flagData;
            $dataset['allColumnsRteDatas'] = $allColumnsRteDatas;
            $dataset['dataRte'] = $dataRte;
            $dataset['flagRteData'] = $flagRteData;

            // dd($dataset);

            // $result = Mail::to(['PKP' => 'parinya.k@lannaagro.com'])
            Mail::to($allStaff)
                ->cc($ccStaff)
                ->send(new TempScadaAllEmail($filename, $date, $dataset));
            // dd($result);
            return $filename;
        }
    }
}

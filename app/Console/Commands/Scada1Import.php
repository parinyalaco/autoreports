<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\TempDataM;
use App\Models\TempDataD;
use App\Models\TempMapColumn;

class Scada1Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'read:scada1data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Read data from scada1 to import to LACO db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $strdate = date('d/m/Y');
        $strtm = date('H:00');
        $date = date('Y-m-d');

        $strtmh = intval(date('H'));
        // $strtmh = 10;
       //  $strtmh = 0;
        if($strtmh == 0){
            $strdate = date('d/m/Y',strtotime('-1 day'));
            $strtms = '23:00';
            $strtmt = '23:59';
        }else{
            $strtms = sprintf('%02d', $strtmh-1).":00";
            $strtmt =  sprintf('%02d', $strtmh - 1) . ":59";

        }
        // $strtm = "10:00";
        // echo $strdate." ".
        // $strtms."-". $strtmt;
        // $strdate = date('03/11/2022');
        // $strtm = date('10:00');
        // $date = date('2022-11-03');

        // $strtm = "12:00";
        try{

        $columnmaplists = TempMapColumn::where('status', 'Active')->pluck('id', 'mapcolumn');

        $dbScada1Obj = DB::connection('scada1');
        $evtObj = $dbScada1Obj->table('TB_NS_EVT_01')
        ->leftJoin('TB_NS_EVT_02', function ($join) {
            $join->on('TB_NS_EVT_01._DATE', '=', 'TB_NS_EVT_02._DATE');
            $join->on('TB_NS_EVT_01._time', '=', 'TB_NS_EVT_02._time');
        })
        ->leftJoin('TB_NS_EVT_03', function ($join) {
                $join->on('TB_NS_EVT_01._DATE', '=', 'TB_NS_EVT_03._DATE');
                $join->on('TB_NS_EVT_01._time', '=', 'TB_NS_EVT_03._time');
            })
        ->leftJoin('TB_NS_EVT_04', function ($join) {
            $join->on('TB_NS_EVT_01._DATE', '=', 'TB_NS_EVT_04._DATE');
            $join->on('TB_NS_EVT_01._time', '=', 'TB_NS_EVT_04._time');
        })
        ->leftJoin('TB_C2_EVT_01', function ($join) {
            $join->on('TB_NS_EVT_01._DATE', '=', 'TB_C2_EVT_01._DATE');
            $join->on('TB_NS_EVT_01._time', '=', 'TB_C2_EVT_01._time');
        })
        ->leftJoin('TB_C3_EVT_01', function ($join) {
            $join->on('TB_NS_EVT_01._DATE', '=', 'TB_C3_EVT_01._DATE');
            $join->on('TB_NS_EVT_01._time', '=', 'TB_C3_EVT_01._time');
        })->select(DB::RAW('tb_ns_evt_01._date,
        avg(cast(tb_ns_evt_01._D01 as float)) as evt_1,
        avg(cast(tb_ns_evt_01._D02 as float)) as evt_2,
        avg(cast(tb_ns_evt_01._D03 as float)) as evt_3,
        avg(cast(tb_ns_evt_01._D04 as float)) as evt_4,
        avg(cast(tb_ns_evt_01._D05 as float)) as evt_5,
        avg(cast(tb_ns_evt_01._D06 as float)) as evt_6,
        avg(cast(tb_ns_evt_01._D07 as float)) as evt_7,
        avg(cast(tb_ns_evt_01._D08 as float)) as evt_8,
        avg(cast(tb_ns_evt_01._D09 as float)) as evt_9,
        avg(cast(tb_ns_evt_02._D01 as float)) as evt_10,
        avg(cast(tb_ns_evt_02._D02 as float)) as evt_11,
        avg(cast(tb_ns_evt_02._D03 as float)) as evt_12,
        avg(cast(tb_ns_evt_02._D04 as float)) as evt_13,
        avg(cast(tb_ns_evt_02._D05 as float)) as evt_14,
        avg(cast(tb_ns_evt_02._D06 as float)) as evt_15,
        avg(cast(tb_ns_evt_02._D07 as float)) as evt_16,
        avg(cast(tb_ns_evt_02._D08 as float)) as evt_17,
        avg(cast(tb_ns_evt_03._D01 as float)) as evt_18,
        avg(cast(tb_ns_evt_03._D02 as float)) as evt_19,
        avg(cast(tb_ns_evt_03._D03 as float)) as evt_20,
        avg(cast(tb_ns_evt_03._D04 as float)) as evt_21,
        avg(cast(tb_ns_evt_03._D05 as float)) as evt_22,
        avg(cast(tb_ns_evt_03._D06 as float)) as evt_23,
        avg(cast(tb_ns_evt_03._D07 as float)) as evt_24,
        avg(cast(tb_ns_evt_03._D08 as float)) as evt_25,
        avg(cast(tb_ns_evt_03._D09 as float)) as evt_26,
        avg(cast(tb_ns_evt_04._D01 as float)) as evt_27,
        avg(cast(tb_ns_evt_04._D02 as float)) as evt_28,
        avg(cast(tb_ns_evt_04._D03 as float)) as evt_29,
        avg(cast(tb_ns_evt_04._D04 as float)) as evt_30,
        avg(cast(tb_ns_evt_04._D05 as float)) as evt_31,
        avg(cast(tb_ns_evt_04._D06 as float)) as evt_32,
        avg(cast(tb_ns_evt_04._D07 as float)) as evt_33,
        avg(cast(tb_ns_evt_04._D08 as float)) as evt_34,
        avg(cast(TB_C2_EVT_01._D01 as float)) as evt_35,
        avg(cast(TB_C2_EVT_01._D02 as float)) as evt_36,
        avg(cast(TB_C2_EVT_01._D03 as float)) as evt_37,
        avg(cast(TB_C2_EVT_01._D04 as float)) as evt_38,
        avg(cast(TB_C2_EVT_01._D05 as float)) as evt_39,
        avg(cast(TB_C2_EVT_01._D06 as float)) as evt_40,
        avg(cast(TB_C2_EVT_01._D07 as float)) as evt_41,
        avg(cast(TB_C2_EVT_01._D08 as float)) as evt_42,
        avg(cast(TB_C2_EVT_01._D09 as float)) as evt_43,
        avg(cast(TB_C3_EVT_01._D01 as float)) as evt_44,
        avg(cast(TB_C3_EVT_01._D02 as float)) as evt_45,
        avg(cast(TB_C3_EVT_01._D03 as float)) as evt_46,
        avg(cast(TB_C3_EVT_01._D04 as float)) as evt_47,
        avg(cast(TB_C3_EVT_01._D05 as float)) as evt_48,
        avg(cast(TB_C3_EVT_01._D06 as float)) as evt_49,
        avg(cast(TB_C3_EVT_01._D07 as float)) as evt_50,
        avg(cast(TB_C3_EVT_01._D08 as float)) as evt_51'));
        $evtdatarw = $evtObj
        ->where('TB_NS_EVT_01._DATE', $strdate)
        ->whereBetween('TB_NS_EVT_01._TIME', [$strtms,$strtmt])
        ->groupBy('tb_ns_evt_01._date')
        ->get();

        $evtdata = [];
        foreach ($evtdatarw as $evtdataObj) {
            for ($i = 1; $i < 52; $i++) {
                $strnamemap = "evt_" . $i;
                if (isset($evtdataObj->$strnamemap)) {
                    $evtdata['EVT ' . $i] = $evtdataObj->$strnamemap;
                }
            }
        }

    //    print_r($evtdata);

        $gauObj = $dbScada1Obj->table('TB_NS_GAU_01')
        ->leftJoin('TB_NS_GAU_02', function ($join) {
            $join->on('TB_NS_GAU_01._DATE', '=', 'TB_NS_GAU_02._DATE');
            $join->on('TB_NS_GAU_01._time', '=', 'TB_NS_GAU_02._time');
        })
        ->leftJoin('TB_NS_GAU_03', function ($join) {
            $join->on('TB_NS_GAU_01._DATE', '=', 'TB_NS_GAU_03._DATE');
            $join->on('TB_NS_GAU_01._time', '=', 'TB_NS_GAU_03._time');
        })
        ->leftJoin('TB_NS_GAU_04', function ($join) {
            $join->on('TB_NS_GAU_01._DATE', '=', 'TB_NS_GAU_04._DATE');
            $join->on('TB_NS_GAU_01._time', '=', 'TB_NS_GAU_04._time');
        })->select(DB::RAW('tb_ns_gau_01._date,
        avg(cast(tb_ns_gau_01._D01 as float)) as GAU_1,
        avg(cast(tb_ns_gau_01._D02 as float)) as GAU_2,
        avg(cast(tb_ns_gau_01._D03 as float)) as GAU_3,
        avg(cast(tb_ns_gau_01._D04 as float)) as GAU_4,
        avg(cast(tb_ns_gau_01._D05 as float)) as GAU_5,
        avg(cast(tb_ns_gau_01._D06 as float)) as GAU_6,
        avg(cast(tb_ns_gau_01._D07 as float)) as GAU_7,
        avg(cast(tb_ns_gau_01._D08 as float)) as GAU_8,
        avg(cast(tb_ns_gau_01._D08 as float)) as GAU_9,
        avg(cast(tb_ns_gau_02._D01 as float)) as GAU_10,
        avg(cast(tb_ns_gau_02._D02 as float)) as GAU_11,
        avg(cast(tb_ns_gau_02._D03 as float)) as GAU_12,
        avg(cast(tb_ns_gau_02._D04 as float)) as GAU_13,
        avg(cast(tb_ns_gau_02._D05 as float)) as GAU_14,
        avg(cast(tb_ns_gau_02._D06 as float)) as GAU_15,
        avg(cast(tb_ns_gau_02._D07 as float)) as GAU_16,
        avg(cast(tb_ns_gau_03._D01 as float)) as GAU_17,
        avg(cast(tb_ns_gau_03._D02 as float)) as GAU_18,
        avg(cast(tb_ns_gau_03._D03 as float)) as GAU_19,
        avg(cast(tb_ns_gau_03._D04 as float)) as GAU_20,
        avg(cast(tb_ns_gau_03._D05 as float)) as GAU_21,
        avg(cast(tb_ns_gau_03._D06 as float)) as GAU_22,
        avg(cast(tb_ns_gau_03._D07 as float)) as GAU_23,
        avg(cast(tb_ns_gau_03._D08 as float)) as GAU_24,
        avg(cast(tb_ns_gau_03._D08 as float)) as GAU_25,
        avg(cast(tb_ns_gau_04._D01 as float)) as GAU_26,
        avg(cast(tb_ns_gau_04._D02 as float)) as GAU_27,
        avg(cast(tb_ns_gau_04._D03 as float)) as GAU_28,
        avg(cast(tb_ns_gau_04._D04 as float)) as GAU_29,
        avg(cast(tb_ns_gau_04._D05 as float)) as GAU_30,
        avg(cast(tb_ns_gau_04._D06 as float)) as GAU_31,
        avg(cast(tb_ns_gau_04._D07 as float)) as GAU_32'));
        $gaudatarw = $gauObj
        ->where('TB_NS_GAU_01._DATE', $strdate)
                ->whereBetween('TB_NS_GAU_01._TIME', [$strtms, $strtmt])
                ->groupBy('TB_NS_GAU_01._date')
        ->get();

        $gaudata = [];
        foreach ($gaudatarw as $gaudataObj) {
            for ($i = 1; $i < 33; $i++) {
                $strnamemap = "GAU_".$i;
                if(isset($gaudataObj->$strnamemap)){
                    $gaudata['GAU'.$i] = $gaudataObj->$strnamemap;
                }
            }
        }

            print_r($evtdata);
            print_r($gaudata);
            //dd($gaudata);

            // if (false) {
        if(!empty($evtdata) || !empty($gaudata)){


            $tmpid = 0;
            $temdatam = TempDataM::where('tempdate', $date)->first();
            if(empty($temdatam)){
                $tmpMaster = array();
                $tmpMaster['filename'] = 'autoreadfromscada1-'.$strdate;
                $tmpMaster['tempdate'] = $date;
                $tmpMaster['result'] = "";
                $tmpMaster['status'] = 'Active';
                $tmpMaster['sheet'] = 'scada1';
                $tmpid = TempDataM::create($tmpMaster)->id;
            }else{
                $tmpid = $temdatam->id;
            }

            //Map data evt
            foreach ($evtdata as $key => $value) {
                if(isset($columnmaplists[$key])){
                    $tmpData = array();
                    $datetimeDATA = \Carbon\Carbon::parse($date. " " . $strtm)->format('Y-m-d H:i');
                    // dd($datetimeDATA);
                    $tmpData['temp_datetime'] = $datetimeDATA;
                    $tmpData['temp_map_column_id'] = $columnmaplists[$key];
                    $tmpData['temp_val'] = $value;
                    $tmpData['temp_data_m_id'] = $tmpid;
                    if (!empty($tmpData) && !empty($tmpData['temp_val'])) {
                        $tempdata = TempDataD::where('temp_datetime', $tmpData['temp_datetime'])
                            ->where('temp_map_column_id', $columnmaplists[$key])
                            ->first();
                        if (!empty($tempdata)) {
                            $tempdata->temp_val = $value;
                            $tempdata->temp_data_m_id = $tmpid;
                            $tempdata->update();
                        } else {
                            TempDataD::create($tmpData);
                        }
                    }
                }
            }

            //Map data gau
            foreach ($gaudata as $key => $value) {
                if (isset($columnmaplists[$key])) {
                    $tmpData = array();
                    $datetimeDATA = \Carbon\Carbon::parse($date . " " . $strtm)->format('Y-m-d H:i');
                    $tmpData['temp_datetime'] = $datetimeDATA;
                    $tmpData['temp_map_column_id'] = $columnmaplists[$key];
                    $tmpData['temp_val'] = $value;
                    $tmpData['temp_data_m_id'] = $tmpid;
                    if (!empty($tmpData) && !empty($tmpData['temp_val'])) {
                        $tempdata = TempDataD::where('temp_datetime', $tmpData['temp_datetime'])
                        ->where('temp_map_column_id', $columnmaplists[$key])
                            ->first();
                        if (!empty($tempdata)) {
                            $tempdata->temp_val = $value;
                            $tempdata->temp_data_m_id = $tmpid;
                            $tempdata->update();
                        } else {
                            TempDataD::create($tmpData);
                        }
                       // dd($tmpData);
                    }
                }
            }


        }else{
      //      $this->line_notify('Error read data from SCADA1 Database');
        }

        } catch( \Illuminate\Database\QueryException $e ){

      //      $this->line_notify('Error read data from SCADA1 Database');

        }


        return 0;
    }

    function line_notify($message, $Token = 'ZNYgOUYhtmogk29qkRAX09zTXxV6YLWF61nDhyySUOC')
    {
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = array('Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . '',);
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        $result = "";
        //Check error
        if (curl_error($chOne)) {
            $result = 'error:' . curl_error($chOne);
        } else {
            // $result_ = json_decode($result, true);
            // $result = "status : " . $result_['status'];
            // $result .= "message : " . $result_['message'];
        }
        curl_close($chOne);
        return $result;
    }
}

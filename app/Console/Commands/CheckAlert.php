<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TempDataM;
use App\Models\TempDataD;
use App\Models\TempMapColumn;
use App\Models\FlagActTime;
use App\Models\Sensor;
use App\Models\SensorData;
use noxil\simplelinealert\SimpleLineAlert;

class CheckAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:checkalert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Check red alert of LACO Temperature';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $checkTime = 3;

        $datamaster = TempMapColumn::where('show', 'Yes')->where('status', 'Active')->get();
        // var_dump($datamaster);
        foreach ($datamaster as $tempm) {
            // var_dump($tempm);
            $flagloop = $checkTime;
            $tmp = "";
            foreach ($tempm->tempds()->orderBy('temp_datetime', 'desc')->limit($checkTime)->get() as $tempdetail) {
                if ($tempdetail->temp_val != null) {

                    $strcond = 'return ' . $tempdetail->temp_val . $tempm->conditions . ';';
                    $pos = strpos($tempm->conditions, '<');
                    if ($pos === false) {
                        $condexplo = explode('ถึง', $tempm->conditions);
                        if(isset($condexplo[0]) && isset($condexplo[1])){
                            $strcond = 'return (' . $tempdetail->temp_val . '>=' . $condexplo[0] . ' && ' . $tempdetail->temp_val . '<=' . $condexplo[1] . ');';
                        }

                    }

                    if (!eval($strcond)) {
                        $flagloop--;
                        $tmp .= " [" . number_format($tempdetail->temp_val, 2, ".", ",") . "]";
                    }
                }
            }
            if ($flagloop == 0) {
                $sla = new SimpleLineAlert();
                $textAlert = $tempm->name . " เงื่อนไข " . $tempm->conditions . " สูงเกิน " . $checkTime . " ครั้ง " . $tmp;
                $sla->sendmessage($textAlert, "");
                // ฝฝvar_dump($tempm);
               // var_dump($textAlert);
            }
        }

        $allColumnsRteDatas = Sensor::where('status', 'Active')->where('show', 'Yes')->orderBy('seq')->get();
        foreach ($allColumnsRteDatas as $tempm) {
            // var_dump($tempm);
            $flagloop = $checkTime;
            $tmp = "";
            foreach ($tempm->tempds()->orderBy('temp_datetime', 'desc')->limit($checkTime)->get() as $tempdetail) {
                if ($tempdetail->value != null && $tempm->conditions != null) {
                    $strcond = 'return ' . $tempdetail->value . $tempm->conditions . ';';
                    $pos = strpos($tempm->conditions, '<');
                    if ($pos === false) {
                        $condexplo = explode('ถึง', $tempm->conditions);
                        //var_dump($condexplo);
                        if (isset($condexplo[0]) && isset($condexplo[1])) {
                            $strcond = 'return (' . $tempdetail->value . '>=' . $condexplo[0] . ' && ' . $tempdetail->value . '<=' . $condexplo[1] . ');';
                        }
                    }

                    if (!eval($strcond)) {
                        $flagloop--;
                        $tmp .= " [" . number_format($tempdetail->value, 2, ".", ",") . "]";
                    }
                }
                if ($flagloop == 0) {
                    $sla = new SimpleLineAlert();
                    $textAlert = $tempm->name . " เงื่อนไข " . $tempm->conditions . " สูงเกิน " . $checkTime . " ครั้ง " . $tmp;
                    $sla->sendmessage($textAlert, "");
                    //var_dump($textAlert);
                }
            }
        }
        return 0;
    }
}

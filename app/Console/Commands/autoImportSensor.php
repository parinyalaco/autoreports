<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use InfluxDB2\Client;
use InfluxDB2\Model\WritePrecision;
use InfluxDB2\Point;
use App\Models\Sensor;

class autoImportSensor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:scadasensor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import scada sensor from InflexDB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        # You can generate a Token from the "Tokens Tab" in the UI
        // $token = 'KBWDG9DYy_GvG1bjIaJDVjcO9ZM4r1GNsc9ygc0gK4iY37PZyJiLB1ZGorKo2lZl9MTNs47zoJMRLYQSf-GXug==';
        // $org = 'demo';
        // $bucket = 'demo-bucket';

        // $client = new Client([
        //     "url" => "http://159.223.48.137:8086",
        //     "token" => $token,
        // ]);

        # You can generate a Token from the "Tokens Tab" in the UI
        $token = '20aAkB3RO7OjfaGaADWiWiScwT5ohsx3EiatcIp8LBrZ4Cb3Z_NGtCRyGeb4ukgafMSm4L4Kk0gDnz5u1uFH1Q==';
        $org = 'org';
        $bucket = 'iot-bucket-data-site03';

        $client = new Client([
            "url" => "http://10.44.65.224:8086",
            "token" => $token,
        ]);

        $data = [];

            $query = "from(bucket: \"{$bucket}\")
        |> range(start: -1h)
        |> filter(fn: (r) => r[\"_measurement\"] == \"temperature\")
        |> filter(fn: (r) => r[\"_field\"] == \"value\")
        |> mean()";
            $tables = $client->createQueryApi()->query($query, $org);

        $failcase = 0;
        while(sizeof($tables) == 0 && $failcase <= 5){
            sleep(15);
            $query = "from(bucket: \"{$bucket}\")
        |> range(start: -1h)
        |> filter(fn: (r) => r[\"_measurement\"] == \"temperature\")
        |> filter(fn: (r) => r[\"_field\"] == \"value\")
        |> mean()";
            $tables = $client->createQueryApi()->query($query, $org);
            $failcase++;
        }


        foreach ($tables as $item) {
            $data[$item->records[0]->values['_measurement']][] = $item->records[0]->values;
        }

        foreach ($data['temperature'] as $sensorObj) {
            $chkSensor = Sensor::where('code', $sensorObj['id'])
            ->where('type', $sensorObj['_measurement'])
            ->where('gateway', $sensorObj['gateway'])->first();
            if(empty($chkSensor)){
                $tmpSensor = [];
                $tmpSensor['code'] = $sensorObj['id'];
                $tmpSensor['type'] = $sensorObj['_measurement'];
                $tmpSensor['gateway'] = $sensorObj['gateway'];
                $tmpSensor['name'] = $sensorObj['name'];
                $tmpSensor['note'] = $sensorObj['name'];
                $tmpSensor['status'] = 'Active';
                Sensor::create($tmpSensor);
            }
        }

        return 0;
    }
}

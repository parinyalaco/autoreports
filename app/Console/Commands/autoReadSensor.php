<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use InfluxDB2\Client;
use InfluxDB2\Model\WritePrecision;
use InfluxDB2\Point;
use App\Models\Sensor;
use App\Models\SensorData;

class autoReadSensor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'read:scadasensor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read scada sensor from InflexDB and Save to MS Sql DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token = '20aAkB3RO7OjfaGaADWiWiScwT5ohsx3EiatcIp8LBrZ4Cb3Z_NGtCRyGeb4ukgafMSm4L4Kk0gDnz5u1uFH1Q==';
        $org = 'org';
        $bucket = 'iot-bucket-data-site03';

        $client = new Client([
            "url" => "http://10.44.65.224:8086",
            "token" => $token,
        ]);

        $query = "from(bucket: \"{$bucket}\")
        |> range(start: -1h)
        |> filter(fn: (r) => r[\"_measurement\"] == \"temperature\")
        |> filter(fn: (r) => r[\"_field\"] == \"value\")
        |> mean()";
        $tables = $client->createQueryApi()->query($query, $org);

        $failcase = 0;
        while (sizeof($tables) == 0 && $failcase <= 5) {
            sleep(15);
            $query = "from(bucket: \"{$bucket}\")
        |> range(start: -1h)
        |> filter(fn: (r) => r[\"_measurement\"] == \"temperature\")
        |> filter(fn: (r) => r[\"_field\"] == \"value\")
        |> mean()";
            $tables = $client->createQueryApi()->query($query, $org);
            $failcase++;
            echo "loop" . $failcase;
        }

        $data = [];
        foreach ($tables as $item) {
            $data[$item->records[0]->values['_measurement']][] = $item->records[0]->values;
        }

        if (empty($data)) {
            $this->line_notify('Error read data from RTE Database');
        } else {
            // echo date('Y-m-d H:00');
            foreach ($data['temperature'] as $sensorObj) {
                $chkSensor = Sensor::where('code', $sensorObj['id'])
                    ->where('type', $sensorObj['_measurement'])
                    ->where('gateway', $sensorObj['gateway'])->first();
                if (!empty($chkSensor)) {
                    $tmpSensorData = [];
                    $tmpSensorData['sensor_id'] = $chkSensor->id;
                    $tmpSensorData['temp_datetime'] = date('Y-m-d H:00');
                    $tmpSensorData['value'] = $sensorObj['_value'];
                    $tmpSensorData['status'] = 'Active';
                    $chkdata = SensorData::where('sensor_id', $tmpSensorData['sensor_id'])
                        ->where('temp_datetime', $tmpSensorData['temp_datetime'])->first();
                    if (empty($chkdata)) {
                        SensorData::create($tmpSensorData);
                    } else {
                        $chkdata->value = $tmpSensorData['value'];
                        $chkdata->update();
                    }
                }
            }
        }
        return 0;
    }

    function line_notify($message, $Token = 'ZNYgOUYhtmogk29qkRAX09zTXxV6YLWF61nDhyySUOC')
    {
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms = trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        // SSL USE
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        //POST
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        $headers = array('Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . '',);
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        $result = "";
        //Check error
        if (curl_error($chOne)) {
            $result = 'error:' . curl_error($chOne);
        } else {
            //  $result_ = json_decode($result, true);
            //    $result = "status : " . $result_['status'];
            //   $result .= "message : " . $result_['message'];
        }
        curl_close($chOne);
        return $result;
    }
}

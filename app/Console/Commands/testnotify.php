<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use noxil\simplelinealert\SimpleLineAlert;

class testnotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:linenotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $sla = new SimpleLineAlert();
        $sla->sendmessage("TEST","");


        // $lineapi = config('LineNotify.linetoken'); // ใส่ token key ที่ได้มา
        // $mms = trim("test"); // ข้อความที่ต้องการส่ง
        // date_default_timezone_set("Asia/Bangkok");
        // $chOne = curl_init();
        // curl_setopt($chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        // // SSL USE
        // curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        // //POST
        // curl_setopt($chOne, CURLOPT_POST, 1);
        // curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=$mms");
        // curl_setopt($chOne, CURLOPT_FOLLOWLOCATION, 1);
        // $headers = array('Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $lineapi . '',);
        // curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        // $result = curl_exec($chOne);
        // $result = "";
        // //Check error
        // if (curl_error($chOne)) {
        //     $result = 'error:' . curl_error($chOne);
        // } else {

        //     $result_ = json_decode($result, true);
        //     dd($result_);
        //     $result = "status : " . $result_['status'];
        //     $result .= "message : " . $result_['message'];
        // }
        // curl_close($chOne);

        return 0;
    }
}

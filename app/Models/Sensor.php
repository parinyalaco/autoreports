<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    use HasFactory;

    protected $fillable = ['code','type', 'gateway','name','note','status','conditions','conditions2','displaycond','show','seq','displayname','coil'];

    public function tempds()
    {
        return $this->hasMany('App\Models\SensorData', 'sensor_id');
    }

}

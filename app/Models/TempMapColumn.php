<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempMapColumn extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','displayname','mapcolumn','typecolumn',
        'conditions', 'conditions2', 'status','seq', 'show',
        'displaycond'
    ];

    public function tempds()
    {
        return $this->hasMany('App\Models\TempDataD', 'temp_map_column_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SensorData extends Model
{
    use HasFactory;

    protected $fillable = ['sensor_id','temp_datetime','value','status'];

    public function sensor()
    {
        return $this->hasOne('App\Models\Sensor', 'id', 'sensor_id');
    }
}

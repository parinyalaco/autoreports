<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempDataM extends Model
{
    use HasFactory;
    protected $fillable = ['filename','tempdate','result','status','sheet'];

    public function tempdatads()
    {
        return $this->hasMany('App\Models\TempDataD', 'temp_data_m_id');
    }
}

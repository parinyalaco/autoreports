<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempDataD extends Model
{
    use HasFactory;
    protected $fillable = ['temp_data_m_id','temp_map_column_id','temp_datetime','temp_val', 'status'];

    public function tempmapcolumn()
    {
        return $this->hasOne('App\Models\TempMapColumn', 'id', 'temp_map_column_id');
    }

}

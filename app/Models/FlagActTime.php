<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlagActTime extends Model
{
    use HasFactory;
    protected $fillable = ['temp_map_column_id','settime','flag','desc','status','sensor_id'];

    public function tempmapcolumn()
    {
        return $this->hasOne('App\Models\TempMapColumn', 'id', 'temp_map_column_id');
    }

    public function sensor()
    {
        return $this->hasOne('App\Models\Sensor', 'id', 'sensor_id');
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TempScadaRteEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $filename, $date, $dataset;

    public function __construct($filename, $date, $dataset)
    {
        $this->filename = $filename;
        $this->date = $date;
        $this->dataset = $dataset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $filewithpath =  storage_path('/') . "/" . $this->filename;

        return $this->view('emails.tempscadarteemail', ['dataset' => $this->dataset, 'date' => $this->date])
            ->from("noreply@lannaagro.com", "QC Temperature Alert RTE")
            ->subject("รายงานอุณหภูมิรายวัน " . $this->date)
            ->attach($filewithpath);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TempMapColumn;
use App\Models\TempDataM;
use App\Models\TempDataD;
use Auth;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd(!empty(Auth::user()));
        if (Auth::user()) {
            return $next($request);
        }
        else{
            return redirect()->route('login.show');
        }

        // return redirect('/');
    }
}

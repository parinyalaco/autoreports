<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }

    public function index() 
    {
        // return view('auth.login');
        return redirect()->route('temp_map_columns.index');
    }
}

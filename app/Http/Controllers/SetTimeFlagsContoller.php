<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FlagActTime;
use App\Models\TempMapColumn;
use App\Models\Sensor;

class SetTimeFlagsContoller extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $settimeflags = FlagActTime::orderBy('id','desc')->paginate($perPage);
        return view('qc.set_time_flags.index',compact('settimeflags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tempmapcolumnlist = TempMapColumn::pluck('mapcolumn','id');
        $sensorlist = Sensor::pluck('name', 'id');
        return view('qc.set_time_flags.create', compact('tempmapcolumnlist', 'sensorlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        FlagActTime::create($requestData);

        return redirect('qc/set_time_flags')->with('flash_message', 'Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $settimeflag = FlagActTime::findOrFail($id);
        return view('qc.set_time_flags.view', compact('settimeflag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settimeflag = FlagActTime::findOrFail($id);
        $tempmapcolumnlist = TempMapColumn::pluck('mapcolumn', 'id');
        $sensorlist = Sensor::pluck('name', 'id');
        return view('qc.set_time_flags.edit ', compact('settimeflag', 'tempmapcolumnlist', 'sensorlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $settimeflag = FlagActTime::findOrFail($id);
        $settimeflag->update($requestData);

        return redirect('qc/set_time_flags')->with('flash_message', 'updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FlagActTime::destroy($id);
        return redirect('qc/set_time_flags')->with('flash_message', 'Deleted!');
    }

    public function clone($id)
    {
        $settimeflag = FlagActTime::findOrFail($id);
        $tempmapcolumnlist = TempMapColumn::pluck('mapcolumn', 'id');
        return view('qc.set_time_flags.clone', compact('tempmapcolumnlist', 'settimeflag'));
    }
}

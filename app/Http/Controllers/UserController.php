<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 25;
        $user = User::orderBy('name')->paginate($perPage);

        return view('auth.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        // dd($requestData);
        $request->validate([
            'name'=>'required|max:255',
            'email'=>'required|email|unique:users,email',    
            'password' => 'required|same:password_confirmation|min:4', 
            'password_confirmation' => 'required',     
        ],
        [
            'name.required'=>"กรุณาระบุชื่อพนักงานด้วยค่ะ",
            'name.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
            'email.required'=>"กรุณาระบุ email ด้วยค่ะ",
            'email.email'=>"ข้อมูลไม่เป็นรูปแบบ email",   
            'email.unique'=>"email นี้ มีในระบบแล้ว",          
            'password.required'=>"กรุณาระบุรหัสผ่านใหม่",
            'password.same'=>"รหัสผ่านใหม่ และ ยืนยันรหัสผ่าน ไม่ตรงกัน",        
            'password_confirmation.required'=>"กรุณายืนยันรหัสผ่านใหม่",
        ]);
        // dd($requestData);
        $update_to['_token'] = $requestData['_token'];
        $update_to['name'] = $requestData['name'];
        $update_to['email'] = $requestData['email'];
        $update_to['password'] = $requestData['password'];
        // dd($update_to);
        $user = User::create($update_to);

        return redirect()->route('user.index')->with('success', 'User updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('auth.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        // dd($requestData);
        $request->validate([
            'name'=>'required|max:255',
            'email'=>'required|email',    
            'password' => 'required|same:password_confirmation|min:4', 
            'password_confirmation' => 'required',     
        ],
        [
            'name.required'=>"กรุณาระบุชื่อพนักงานด้วยค่ะ",
            'name.max'=>"ระบุข้อมูลเกิน 255 ตัวอักษร",
            'email.required'=>"กรุณาระบุ email ด้วยค่ะ",
            'email.email'=>"ข้อมูลไม่เป็นรูปแบบ email",            
            'password.required'=>"กรุณาระบุรหัสผ่านใหม่",
            'password.same'=>"รหัสผ่านใหม่ และ ยืนยันรหัสผ่าน ไม่ตรงกัน",        
            'password_confirmation.required'=>"กรุณายืนยันรหัสผ่านใหม่",
        ]);
        // dd($requestData);
        $update_to['_token'] = $requestData['_token'];
        $update_to['_method'] = $requestData['_method'];
        $update_to['name'] = $requestData['name'];
        $update_to['email'] = $requestData['email'];
        $update_to['password'] = $requestData['password'];
        // dd($update_to);
        $user = User::findOrFail($id);
        $user->update($update_to);

        return redirect()->route('user.index')->with('success', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);        
        $user->delete();
        return redirect()->route('user.index')->with('success', 'User delete!');
    }
}

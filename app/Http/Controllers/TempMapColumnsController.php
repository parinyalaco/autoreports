<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TempMapColumn;

class TempMapColumnsController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tempmapcolumns = TempMapColumn::orderBy('seq')->paginate($perPage);
        } else {
            $tempmapcolumns = TempMapColumn::orderBy('seq')->paginate($perPage);
        }

        return view('qc.temp_map_columns.index', compact('tempmapcolumns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('qc.temp_map_columns.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        TempMapColumn::create($requestData);

        return redirect('qc/temp_map_columns')->with('flash_message', 'TempMapColumn added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tempmapcolumn = TempMapColumn::findOrFail($id);

        return view('qc.temp_map_columns.view', compact('tempmapcolumn'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tempmapcolumn = TempMapColumn::findOrFail($id);

        return view('qc.temp_map_columns.edit', compact('tempmapcolumn'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $tempmapcolumn = TempMapColumn::findOrFail($id);
        $tempmapcolumn->update($requestData);

        return redirect('qc/temp_map_columns')->with('flash_message', 'Dep updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

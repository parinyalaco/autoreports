<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TempDataM;
use App\Models\TempDataD;
use App\Models\TempMapColumn;
use App\Models\FlagActTime;
use App\Models\SensorData;
use App\Models\Sensor;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

class TempsController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $tampOBj = new TempDataM();

        if (!empty($keyword)) {
            $temps = $tampOBj::where('tempdate',$keyword)
                ->groupBy('tempdate')
                ->where('status', 'Active')
                ->selectRaw('tempdate, count(id) as tabledata')->OrderBy('tempdate','desc')->paginate($perPage);
        } else {
            $temps = $tampOBj::groupBy('tempdate')
                ->where('status', 'Active')
                ->selectRaw('tempdate, count(id) as tabledata')->OrderBy('tempdate', 'desc')->paginate($perPage);
        }



        return view('qc.temps.index', compact('temps'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($date)
    {
        $datamaster = TempDataM::where('tempdate',$date)->where('status', 'Active')->pluck('id','id');

        $alltemps = TempDataD::whereIn('temp_data_m_id', $datamaster)->get();
        $parametermaps = TempMapColumn::where('status','Active')->where('show', 'Yes')->orderBy('seq','asc')->get();

        $allData = array();
        foreach ($alltemps as $alltemp) {
            $allData[$alltemp->tempmapcolumn->name][date('H:i',strtotime($alltemp->temp_datetime))] = $alltemp;
        }

        $timeList = array();
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $timeList[] = "0" . $i . ":00";
            } else {
                $timeList[] = $i . ":00";
            }
        }

        $allFlagDatas = FlagActTime::where('status', 'Active')->get();
        $flagData = array();
        foreach ($allFlagDatas as $flagDataObj) {
            $flagData[$flagDataObj->temp_map_column_id][$flagDataObj->settime] = $flagDataObj->flag;
        }

        $allColumnsRteDatas = Sensor::where('status', 'Active')->where('show', 'Yes')->orderBy('seq')->get();
        $allFlagRteDatas = FlagActTime::where('status', 'Active')->where('sensor_id', '>', 0)->get();
        $flagRteData = array();
        foreach ($allFlagRteDatas as $flagDataObj) {
            $flagRteData[$flagDataObj->temp_map_column_id][$flagDataObj->settime] = $flagDataObj->flag;
        }

        $dataRte = array();
        $rawRteData = SensorData::where('temp_datetime', '>=', $date . " 00:00")->where('temp_datetime', '<=', $date . " 23:59")->get();
        if ($rawRteData->count() > 0) {
            foreach ($rawRteData as $tempdatad) {
                //foreach ($masterdata->tempdatads as $tempdatad) {
                $dataRte[$tempdatad->sensor_id][date("Y-m-d H:i", strtotime($tempdatad->temp_datetime))] = $tempdatad->value;
                //}
            }
        }

        return view('qc.temps.show', compact('allData', 'timeList', 'parametermaps','date', 'flagData',
        'allColumnsRteDatas',
            'flagRteData', 'dataRte'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function excel($date)
    {
        $datamaster = TempDataM::where('tempdate', $date)->where('status', 'Active')->pluck('id', 'id');

        $alltemps = TempDataD::whereIn('temp_data_m_id', $datamaster)->get();
        $parametermaps = TempMapColumn::where('status', 'Active')->where('show', 'Yes')->orderBy('seq', 'asc')->get();

        $data = array();
        foreach ($alltemps as $alltemp) {
            $data[$alltemp->tempmapcolumn->id][date('H:i', strtotime($alltemp->temp_datetime))] = $alltemp;
        }

        $timeList = array();
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $timeList[] = "0" . $i . ":00";
            } else {
                $timeList[] = $i . ":00";
            }
        }

        $allFlagDatas = FlagActTime::where('status', 'Active')->get();
        $flagData = array();
        foreach ($allFlagDatas as $flagDataObj) {
            $flagData[$flagDataObj->temp_map_column_id][$flagDataObj->settime] = $flagDataObj->flag;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, "รายงานอุณหภูมิห้อง วันที่ " . $date);

        $sheet->setCellValueByColumnAndRow(1, 3, "รายละเอียด");
        $sheet->setCellValueByColumnAndRow(2, 3, "Coil");
        $sheet->setCellValueByColumnAndRow(3, 3, " Temp SPEC (˚C)");
        foreach ($timeList as $key => $value) {
            $sheet->setCellValueByColumnAndRow(4 + $key, 3, $value);
        }

        $rowdata = 4;
        $loopdata = 0;
        $codeloc = "";
        foreach ($parametermaps as $ColumnsData) {
            $sheet->setCellValueByColumnAndRow(1, $rowdata + $loopdata, $ColumnsData->displayname);
            $sheet->setCellValueByColumnAndRow(2, $rowdata + $loopdata, $ColumnsData->mapcolumn);
            $sheet->setCellValueByColumnAndRow(3, $rowdata + $loopdata, $ColumnsData->displaycond);

            foreach ($timeList as $key => $value) {
                if (isset($data[$ColumnsData->id]) && isset($data[$ColumnsData->id][$value]->temp_val)) {

                    if (isset($flagData[$ColumnsData->id][$value])) {
                        $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, number_format($data[$ColumnsData->id][$value]->temp_val, 2, '.', '') . "," . $flagData[$ColumnsData->id][$value]);
                    } else {
                        $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $data[$ColumnsData->id][$value]->temp_val);
                    }

                    $codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();

                    if (!empty($ColumnsData->conditions2)) {
                        if (!empty($data[$ColumnsData->id][$value]->temp_val)) {
                            //echo $dat a[$ColumnsData->id][$value] . $ColumnsData->conditions;
                            $strcond = "return " . $data[$ColumnsData->id][$value]->temp_val . $ColumnsData->conditions . ";";
                            $pos = strpos($ColumnsData->conditions2, '<');
                            if ($pos === false) {
                                $condexplo = explode("ถึง", $ColumnsData->conditions2);
                                $strcond = "return (" . $data[$ColumnsData->id][$value]->temp_val . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$value]->temp_val . "<=" . $condexplo[1] . ");";
                            }
                            if (eval($strcond)) {
                                //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                //echo $codeloc;
                                $sheet->getStyle($codeloc)
                                    ->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setARGB('FFFFFF00');
                            } else {
                                if (!empty($ColumnsData->conditions)) {

                                    if (!empty($data[$ColumnsData->id][$value]->temp_val)) {
                                        //echo $data[$ColumnsData->id][$value]->temp_val . $ColumnsData->conditions;
                                        $strcond = "return " . $data[$ColumnsData->id][$value]->temp_val . $ColumnsData->conditions . ";";
                                        $pos = strpos($ColumnsData->conditions, '<');
                                        if ($pos === false) {
                                            $condexplo = explode("ถึง", $ColumnsData->conditions);
                                            $strcond = "return (" . $data[$ColumnsData->id][$value]->temp_val . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$value]->temp_val . "<=" . $condexplo[1] . ");";
                                        }
                                        if (!eval($strcond)) {
                                            //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                            //echo $codeloc;
                                            $sheet->getStyle($codeloc)
                                                ->getFill()
                                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                                ->getStartColor()->setARGB('FFFF0000');
                                        }
                                    } else {
                                        $sheet->getStyle($codeloc)
                                            ->getFill()
                                            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                            ->getStartColor()->setARGB('FF000000');
                                    }
                                }
                            }
                        } else {
                            $sheet->getStyle($codeloc)
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FF000000');
                        }
                    } else {
                        if (!empty($ColumnsData->conditions)) {

                            if (!empty($data[$ColumnsData->id][$value]->temp_val)) {
                                //echo $data[$ColumnsData->id][$value]->temp_val . $ColumnsData->conditions;
                                $strcond = "return " . $data[$ColumnsData->id][$value]->temp_val . $ColumnsData->conditions . ";";
                                $pos = strpos($ColumnsData->conditions, '<');
                                if ($pos === false) {
                                    $condexplo = explode("ถึง", $ColumnsData->conditions);
                                    $strcond = "return (" . $data[$ColumnsData->id][$value]->temp_val . ">=" . $condexplo[0] . " && " . $data[$ColumnsData->id][$value]->temp_val . "<=" . $condexplo[1] . ");";
                                }
                                if (!eval($strcond)) {
                                    //$codeloc = $sheet->getCellByColumnAndRow(4 + $key, $rowdata + $loopdata)->getCoordinate();
                                    //echo $codeloc;
                                    $sheet->getStyle($codeloc)
                                        ->getFill()
                                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFFF0000');
                                }
                            } else {
                                $sheet->getStyle($codeloc)
                                    ->getFill()
                                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                    ->getStartColor()->setARGB('FF000000');
                            }
                        }
                    }
                } else {
                    if (isset($flagData[$ColumnsData->id][$value])) {
                        $sheet->setCellValueByColumnAndRow(4 + $key, $rowdata + $loopdata, $flagData[$ColumnsData->id][$value]);
                    }
                }
            }
            $loopdata++;
        }
        $sheet->getStyle('A3:' . $codeloc)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new Color('000000'));

        $writer = new Xlsx($spreadsheet);

        $filename = "exportdata-" . date('yymmdd-hi') . ".xlsx";

        $writer->save('storage/' . $filename);

        return response()->download('storage/' . $filename);
    }
}

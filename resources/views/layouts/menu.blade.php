<ul class="menu">

    @if (Auth::guest())
        <li class="sidebar-item active ">
            <a href="{{ route('login.perform') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                <span>Login</span>
            </a>
        </li>
    @else
        <li class="sidebar-item">
            {{ Auth::user()->name }}
        </li>
        <li class="sidebar-item">
            <a href="{{ route('user.index') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                <span>User</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('temp_map_columns.index') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                <span>จัดการ Column Map กับ Excel Scada</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('sensors.index') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                <span>จัดการ Sensor กับ Flex Scada</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a href="{{ route('set_time_flags.index') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                <span>จัดการ Flag Time</span>
            </a>
        </li>

        <li class="sidebar-item">
            <a href="{{ route('temps.index') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                <span>ข้อมูลอุณหภูมิ</span>
            </a>
        </li>

        
        <li class="sidebar-item">
            <a href="{{ route('logout.perform') }}">Logout</a>
        </li>

        {{-- <li class="sidebar-item  has-sub">
            <a href="#" class='sidebar-link'>
                <i class="bi bi-stack"></i>
                <span>รายงาน</span>
            </a>
            <ul class="submenu ">
                <li class="submenu-item ">
                    <a href="{{ url('/reports/exportdata') }}">Exports</a>
                </li>
            </ul>
        </li>
        @guest

        @else 
        
                <li class="sidebar-item ">
                    <a  class='sidebar-link' href="#">Change Pass</a>
                </li>
        <li class="sidebar-item">
            <!-- Authentication -->
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a  class='sidebar-link' href="{{ route('logout') }}" onclick="event.preventDefault();
                        this.closest('form').submit();">
                    {{ __('Logout') }}
                </a>
            </form>
        </li>
        @endguest--}}
        
        
    @endif
</ul>

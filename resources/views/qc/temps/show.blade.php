@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('อุณหภูมิประจำวันที่ ' . $date) }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>รายละเอียด</th>
                            <th>Coil</th>
                            <th>Temp SPEC (˚C)</th>
                            @foreach ($timeList as $keytime => $valuetime)
                                <th>{{ $valuetime }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($parametermaps as $parammap)
                            <tr>
                                <td>{{ $parammap->displayname }}</td>
                                <td>{{ $parammap->mapcolumn }}</td>
                                <td>{{ $parammap->displaycond }}</td>
                                @foreach ($timeList as $keytime => $valuetime)
                                    @if (isset($allData[$parammap->name]) && isset($allData[$parammap->name][$valuetime]))
                                        @if (!empty($parammap->conditions2))
                                            @if (!empty($allData[$parammap->name][$valuetime]->temp_val))
                                                @php
                                                    $strcond = 'return ' . $allData[$parammap->name][$valuetime]->temp_val . $parammap->conditions2 . ';';
                                                    $pos = strpos($parammap->conditions2, '<');
                                                    if ($pos === false) {
                                                        $condexplo = explode('ถึง', $parammap->conditions2);
                                                        $strcond = 'return (' . $allData[$parammap->name][$valuetime]->temp_val . '>=' . $condexplo[0] . ' && ' . $allData[$parammap->name][$valuetime]->temp_val . '<=' . $condexplo[1] . ');';
                                                    }
                                                    // /echo $strcond;
                                                @endphp
                                                @if (eval($strcond))
                                                    <td style="background-color:#ffff00;">
                                                        {{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                    @else
                                                        @if (!empty($parammap->conditions))
                                                            @if (!empty($allData[$parammap->name][$valuetime]->temp_val))
                                                                @php
                                                                    $strcond = 'return ' . $allData[$parammap->name][$valuetime]->temp_val . $parammap->conditions . ';';
                                                                    $pos = strpos($parammap->conditions, '<');
                                                                    if ($pos === false) {
                                                                        $condexplo = explode('-', $parammap->conditions);
                                                                        $strcond = 'return (' . $allData[$parammap->name][$valuetime]->temp_val . '>=' . $condexplo[0] . ' && ' . $allData[$parammap->name][$valuetime]->temp_val . '<=' . $condexplo[1] . ');';
                                                                    }
                                                                @endphp
                                                                @if (!eval($strcond))
                                                                    <td style="background-color:#ff0000;">
                                                                        {{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                                @else
                                                                    <td>{{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                                @endif
                                                            @else
                                                                <td>{{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                            @endif
                                                        @else
                                                            <td>{{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                        @endif
                                                    @endif
                                                @else
                                                    <td>{{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                @endif
                                            @else
                                                @if (!empty($parammap->conditions))
                                                    @if (!empty($allData[$parammap->name][$valuetime]->temp_val))
                                                        @php
                                                            $strcond = 'return ' . $allData[$parammap->name][$valuetime]->temp_val . $parammap->conditions . ';';
                                                            $pos = strpos($parammap->conditions, '<');
                                                            if ($pos === false) {
                                                                $condexplo = explode('ถึง', $parammap->conditions);
                                                                $strcond = 'return (' . $allData[$parammap->name][$valuetime]->temp_val . '>=' . $condexplo[0] . ' && ' . $allData[$parammap->name][$valuetime]->temp_val . '<=' . $condexplo[1] . ');';
                                                            }
                                                        @endphp
                                                        @if (!eval($strcond))
                                                            <td style="background-color:#ff0000;">
                                                                {{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                            @else
                                                            <td>{{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                        @endif
                                                    @else
                                                        <td>{{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                    @endif
                                                @else
                                                    <td>{{ number_format($allData[$parammap->name][$valuetime]->temp_val, 2, '.', ',') }}
                                                @endif
                                        @endif
                                    @else
                                        <td>-
                                    @endif
                                    @if (isset($flagData[$parammap->id][$valuetime]))
                                        ,{{ $flagData[$parammap->id][$valuetime] }}
                                    @endif
                                        </td>
                                @endforeach
                            </tr>
                        @endforeach

                        @foreach ($allColumnsRteDatas as $ColumnsData)
                            <tr>
                                <td>{{ $ColumnsData->displayname }}</td>
                                <td>{{ $ColumnsData->coil }}</td>
                                <td>{{ $ColumnsData->displaycond }}</td>
                                @foreach ($timeList as $keytime => $valuetime)
                                    @if (isset($dataRte[$ColumnsData->id]) && isset($dataRte[$ColumnsData->id][$date . ' ' . $valuetime]))
                                        @if (!empty($ColumnsData->conditions2))
                                            @if (!empty($dataRte[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                @php
                                                    $strcond = 'return ' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . $ColumnsData->conditions . ';';
                                                    $pos = strpos($ColumnsData->conditions2, '<');
                                                    if ($pos === false) {
                                                        $condexplo = explode('ถึง', $ColumnsData->conditions2);
                                                        $strcond = 'return (' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . '>=' . $condexplo[0] . ' && ' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . '<=' . $condexplo[1] . ');';
                                                    }
                                                @endphp
                                                @if (eval($strcond))
                                                    <td  style="background-color:#ffff00;">
                                                        {{ number_format($dataRte[$ColumnsData->id][$date . ' ' . $valuetime], 2, '.', ',') }}
                                                        @if (isset($flagRteData[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                            ,{{ $flagRteData[$ColumnsData->id][$date . ' ' . $valuetime] }}
                                                        @endif
                                                    </td>
                                                @else
                                                    @if (!empty($ColumnsData->conditions))
                                                        @if (!empty($dataRte[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                            @php
                                                                $strcond = 'return ' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . $ColumnsData->conditions . ';';
                                                                $pos = strpos($ColumnsData->conditions, '<');
                                                                if ($pos === false) {
                                                                    $condexplo = explode('-', $ColumnsData->conditions);
                                                                    $strcond = 'return (' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . '>=' . $condexplo[0] . ' && ' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . '<=' . $condexplo[1] . ');';
                                                                }
                                                            @endphp
                                                            @if (!eval($strcond))
                                                                <td  style="background-color:#ff0000;">
                                                                    {{ number_format($dataRte[$ColumnsData->id][$date . ' ' . $valuetime], 2, '.', ',') }}
                                                                    @if (isset($flagRteData[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                                        ,{{ $flagRteData[$ColumnsData->id][$date . ' ' . $valuetime] }}
                                                                    @endif
                                                                </td>
                                                            @else
                                                                <td>{{ number_format($dataRte[$ColumnsData->id][$date . ' ' . $valuetime], 2, '.', ',') }}
                                                                    @if (isset($flagRteData[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                                        ,{{ $flagRteData[$ColumnsData->id][$date . ' ' . $valuetime] }}
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @else
                                                            <td class='blackbg'>-</td>
                                                        @endif
                                                    @else
                                                        <td>{{ number_format($dataRte[$ColumnsData->id][$date . ' ' . $valuetime], 2, '.', ',') }}
                                                            @if (isset($flagRteData[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                                ,{{ $flagRteData[$ColumnsData->id][$date . ' ' . $valuetime] }}
                                                            @endif
                                                        </td>
                                                    @endif
                                                @endif
                                            @else
                                                <td class='blackbg'>-</td>
                                            @endif
                                        @else
                                            @if (!empty($ColumnsData->conditions))
                                                @if (!empty($dataRte[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                    @php
                                                        $strcond = 'return ' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . $ColumnsData->conditions . ';';
                                                        $pos = strpos($ColumnsData->conditions, '<');
                                                        if ($pos === false) {
                                                            $condexplo = explode('ถึง', $ColumnsData->conditions);
                                                            $strcond = 'return (' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . '>=' . $condexplo[0] . ' && ' . $dataRte[$ColumnsData->id][$date . ' ' . $valuetime] . '<=' . $condexplo[1] . ');';
                                                        }
                                                    @endphp
                                                    @if (!eval($strcond))
                                                        <td  style="background-color:#ff0000;">
                                                            {{ number_format($dataRte[$ColumnsData->id][$date . ' ' . $valuetime], 2, '.', ',') }}
                                                            @if (isset($flagRteData[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                                ,{{ $flagRteData[$ColumnsData->id][$date . ' ' . $valuetime] }}
                                                            @endif
                                                        </td>
                                                    @else
                                                        <td>{{ number_format($dataRte[$ColumnsData->id][$date . ' ' . $valuetime], 2, '.', ',') }}
                                                            @if (isset($flagRteData[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                                ,{{ $flagRteData[$ColumnsData->id][$date . ' ' . $valuetime] }}
                                                            @endif
                                                        </td>
                                                    @endif
                                                @else
                                                    <td class='blackbg'>-</td>
                                                @endif
                                            @else
                                                <td>{{ number_format($dataRte[$ColumnsData->id][$date . ' ' . $valuetime], 2, '.', ',') }}
                                                    @if (isset($flagRteData[$ColumnsData->id][$date . ' ' . $valuetime]))
                                                        ,{{ $flagRteData[$ColumnsData->id][$date . ' ' . $valuetime] }}
                                                    @endif
                                                </td>
                                            @endif
                                        @endif
                                    @else
                                        <td>-</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

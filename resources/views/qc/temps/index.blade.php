@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('ข้อมูลอุณภูมิแยกตามวัน') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                            <div class="pagination-wrapper"> {!! $temps->render('vendor\pagination\bootstrap-4') !!} </div>
                   
                        <table class="table table-bordered">
                            <tr>
                                <th>Date</th>
                                <th>จำนวน ตาราง</th>
                                <th></th>
                            </tr>
                            @foreach ($temps as $temp)
                                <tr>
                                    <td>{{ $temp->tempdate }}</td>
                                    <td>{{ $temp->tabledata }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('temps.show', $temp->tempdate) }}">แสดง</a>
                                         <a class="btn btn-info" href="{{ route('temps.excel', $temp->tempdate) }}">Excel</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                         <div class="pagination-wrapper"> {!! $temps->render('vendor\pagination\bootstrap-4') !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

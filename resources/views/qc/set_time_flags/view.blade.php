@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการประเภทกล่องแตก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('temp_map_columns.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แสดงประเภทกล่องแตก') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <td>{{ $tempmapcolumn->name }}</td>
                            </tr>
                            <tr>
                                <th>Displayname</th>
                                <td>{{ $tempmapcolumn->displayname }}</td>
                            </tr>
                            <tr>
                                <th>Map Column</th>
                                <td>{{ $tempmapcolumn->mapcolumn }}</td>
                            </tr>
                            <tr>
                                <th>Type Column</th>
                                <td>{{ $tempmapcolumn->typecolumn }}</td>
                            </tr>
                            <tr>
                                <th>Show</th>
                                <td> <div class="col-md-6">
                                    {{ Form::select('show', array('Yes'=>'Yes','No'=>'No'), $tempmapcolumn->show, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}                                    
                            </tr>
                            <tr>
                                <th>Seq</th>
                                <td>{{ $tempmapcolumn->seq }}</td>
                            </tr>
                            <tr>
                                <th>Condition</th>
                                <td>{{ $tempmapcolumn->conditions }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ $tempmapcolumn->status }}</td>
                            </tr>
                            
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('temp_map_columns.edit', $tempmapcolumn->id) }}">แก้ไข</a>
                                    <form action="{{ route('temp_map_columns.destroy', $tempmapcolumn->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">ลบ</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

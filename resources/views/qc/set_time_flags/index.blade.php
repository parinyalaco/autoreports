@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Flag Time') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('set_time_flags.create') }}">สร้าง Flag Time</a>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                            <div class="pagination-wrapper"> {!! $settimeflags->render('vendor\pagination\bootstrap-4') !!} </div>
                   
                        <table class="table table-bordered">
                            <tr>
                                <th>Sensor / Sensor RTE</th>
                                <th>เวลา</th>
                                <th>Flag</th>
                                <th>สถานะ</th>
                                <th></th>
                            </tr>
                            @foreach ($settimeflags as $settimeflag)
                                <tr>
                                    <td>{{ $settimeflag->tempmapcolumn->mapcolumn ?? '-' }} / {{ $settimeflag->sensor->coil ?? '-' }} </td>
                                    <td>{{ $settimeflag->settime }}</td>
                                    <td>{{ $settimeflag->flag }}</td>
                                    <td>{{ $settimeflag->status }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('set_time_flags.show', $settimeflag->id) }}">แสดง</a>
                                        <a class="btn btn-primary" href="{{ route('set_time_flags.edit', $settimeflag->id) }}">แก้ไข</a>
                                        <a class="btn btn-secondary" href="{{ route('set_time_flags.clone', $settimeflag->id) }}">clone</a>
                                        <form action="{{ route('set_time_flags.destroy', $settimeflag->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                         <div class="pagination-wrapper"> {!! $settimeflags->render('vendor\pagination\bootstrap-4') !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

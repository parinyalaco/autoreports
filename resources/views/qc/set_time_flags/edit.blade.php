@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Flag Time') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('set_time_flags.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แก้ไข Column Map กับ Excel Scada') }}</div>

                    <div class="card-body">
                        <form action="{{ route('set_time_flags.update', $settimeflag->id) }}" method="POST">
                            @csrf

                            @method('PUT')
                            
                            <div class="form-group row">
                                <label for="temp_map_column_id" class="col-md-3 col-form-label text-md-right">{{ __('Sensor') }}</label>

                                <div class="col-md-6">
                                    {!! Form::select('temp_map_column_id', $tempmapcolumnlist, $settimeflag->temp_map_column_id, ['placeholder'=>'--Select--','class'=>'form-control']) !!}

                                    @error('temp_map_column_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sensor_id" class="col-md-3 col-form-label text-md-right">{{ __('Sensor RTE') }}</label>

                                <div class="col-md-6">
                                    {!! Form::select('sensor_id', $sensorlist, $settimeflag->sensor_id, ['placeholder'=>'--Select--','class'=>'form-control']) !!}

                                    @error('sensor_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="settime" class="col-md-3 col-form-label text-md-right">{{ __('Set Time (HH:00) Ex. 02:00 ') }}</label>

                                <div class="col-md-6">
                                    <input id="settime" type="text" class="form-control @error('settime') is-invalid @enderror"
                                        name="settime" value="{{ $settimeflag->settime }}" required autofocus>

                                    @error('settime')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="flag" class="col-md-3 col-form-label text-md-right">{{ __('Flag') }}</label>

                                <div class="col-md-6">
                                    <input id="flag" type="text" class="form-control @error('flag') is-invalid @enderror"
                                        name="flag" value="{{ $settimeflag->flag }}" required autofocus>

                                    @error('flag')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="desc" class="col-md-3 col-form-label text-md-right">{{ __('รายละเอียด') }}</label>

                                <div class="col-md-6">
                                    <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror"
                                        name="desc" value="{{ $settimeflag->desc }}" required autofocus>

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-6">
                                    <input id="status" type="text" class="form-control @error('status') is-invalid @enderror"
                                        name="status" value="{{ $settimeflag->status }}" autofocus>

                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('แก้ไข') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

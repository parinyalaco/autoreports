@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Column Map กับ Excel Scada') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('sensors.create') }}">สร้าง Sensor</a>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                            <div class="pagination-wrapper"> {!! $sensors->render('vendor\pagination\bootstrap-4') !!} </div>
                   
                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <th>เงื่อนไข RED</th>
                                <th>เงื่อนไข Yellow</th>
                                <th>ลำดับ</th>
                                <th>แสดง</th>
                                <th></th>
                            </tr>
                            @foreach ($sensors as $sensor)
                                <tr>
                                    <td>{{ $sensor->displayname }} / {{ $sensor->coil }}</td>
                                    <td>{{ $sensor->conditions }}</td>
                                    <td>{{ $sensor->conditions2 }}</td>
                                    <td>{{ $sensor->seq }}</td>
                                    <td>{{ $sensor->show }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('sensors.show', $sensor->id) }}">แสดง</a>
                                        <a class="btn btn-primary" href="{{ route('sensors.edit', $sensor->id) }}">แก้ไข</a>
                                        <form action="{{ route('sensors.destroy', $sensor->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                         <div class="pagination-wrapper"> {!! $sensors->render('vendor\pagination\bootstrap-4') !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Column Map กับ Excel Scada') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('temp_map_columns.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้าง Column Map') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('temp_map_columns.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('ชื่อ') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="displayname" class="col-md-3 col-form-label text-md-right">{{ __('Displayname') }}</label>

                                <div class="col-md-6">
                                    <input id="displayname" type="text" class="form-control @error('displayname') is-invalid @enderror"
                                        name="displayname" value="" required autofocus>

                                    @error('displayname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="mapcolumn" class="col-md-3 col-form-label text-md-right">{{ __('Map Column') }}</label>

                                <div class="col-md-6">
                                    <input id="mapcolumn" type="text" class="form-control @error('mapcolumn') is-invalid @enderror"
                                        name="mapcolumn" value="" required autofocus>

                                    @error('mapcolumn')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="typecolumn" class="col-md-3 col-form-label text-md-right">{{ __('Type Column') }}</label>

                                <div class="col-md-6">
                                    <input id="typecolumn" type="text" class="form-control @error('typecolumn') is-invalid @enderror"
                                        name="typecolumn" value="" required autofocus>

                                    @error('typecolumn')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="show" class="col-md-3 col-form-label text-md-right">{{ __('Show') }}</label>

                                <div class="col-md-6">
                                    {{ Form::select('show', array('Yes'=>'Yes','No'=>'No'), 'Yes', ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    

                                    @error('show')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="seq" class="col-md-3 col-form-label text-md-right">{{ __('Seq') }}</label>

                                <div class="col-md-6">
                                    <input id="seq" type="number" class="form-control @error('typecolumn') is-invalid @enderror"
                                        name="seq" value="1" required autofocus>

                                    @error('seq')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="conditions" class="col-md-3 col-form-label text-md-right">{{ __('Condition') }}</label>

                                <div class="col-md-6">
                                    <input id="conditions" type="text" class="form-control @error('conditions') is-invalid @enderror"
                                        name="conditions" value="" autofocus>

                                    @error('conditions')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="conditions2" class="col-md-3 col-form-label text-md-right">{{ __('Condition 2') }}</label>

                                <div class="col-md-6">
                                    <input id="conditions2" type="text" class="form-control @error('conditions2') is-invalid @enderror"
                                        name="conditions2" value="" autofocus>

                                    @error('conditions2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-6">
                                    <input id="status" type="text" class="form-control @error('status') is-invalid @enderror"
                                        name="status" value="Active" autofocus>

                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('สร้าง') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Sensor') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('sensors.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แสดงประเภทกล่องแตก') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Code</th>
                                <td>{{ $sensor->code }}</td>
                            </tr>
                            <tr>
                                <th>Type</th>
                                <td>{{ $sensor->type }}</td>
                            </tr>
                            <tr>
                                <th>Gateway</th>
                                <td>{{ $sensor->gateway }}</td>
                            </tr>
                            <tr>
                                <th>ชื่อ</th>
                                <td>{{ $sensor->name }}</td>
                            </tr>
                            <tr>
                                <th>Display Name</th>
                                <td>{{ $sensor->displayname }}</td>
                            </tr>
                            <tr>
                                <th>Coil</th>
                                <td>{{ $sensor->coil }}</td>
                            </tr>
                            <tr>
                                <th>Note</th>
                                <td>{{ $sensor->note }}</td>
                            </tr>
                            <tr>
                                <th>Show</th>
                                <td> <div class="col-md-6">
                                    {{ Form::select('show', array('Yes'=>'Yes','No'=>'No'), $sensor->show, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}                                    
                            </tr>
                            <tr>
                                <th>Seq</th>
                                <td>{{ $sensor->seq }}</td>
                            </tr>
                            <tr>
                                <th>Condition Red</th>
                                <td>{{ $sensor->conditions }}</td>
                            </tr>
                            <tr>
                                <th>Condition Yellow</th>
                                <td>{{ $sensor->conditions2 }}</td>
                            </tr>
                            <tr>
                                <th>Condition Display</th>
                                <td>{{ $sensor->displaycond }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ $sensor->status }}</td>
                            </tr>
                            
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('sensors.edit', $sensor->id) }}">แก้ไข</a>
                                    <form action="{{ route('sensors.destroy', $sensor->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">ลบ</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

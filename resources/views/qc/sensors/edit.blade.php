@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Column Map กับ Excel Scada') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('sensors.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แก้ไข Sensor กับ RTE Scada') }}</div>

                    <div class="card-body">
                        <form action="{{ route('sensors.update', $sensor->id) }}" method="POST">
                            @csrf

                            @method('PUT')
                            <div class="form-group row">
                                <label for="code" class="col-md-3 col-form-label text-md-right">{{ __('Code') }}</label>

                                <div class="col-md-6">
                                    <input id="code" type="text" class="form-control @error('code') is-invalid @enderror"
                                        name="code" value="{{ $sensor->code }}" required autocomplete="code" autofocus>

                                    @error('code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type" class="col-md-3 col-form-label text-md-right">{{ __('type') }}</label>

                                <div class="col-md-6">
                                    <input id="type" type="text" class="form-control @error('type') is-invalid @enderror"
                                        name="type" value="{{ $sensor->type }}" required autocomplete="type" autofocus>

                                    @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gateway" class="col-md-3 col-form-label text-md-right">{{ __('gateway') }}</label>

                                <div class="col-md-6">
                                    <input id="gateway" type="text" class="form-control @error('gateway') is-invalid @enderror"
                                        name="gateway" value="{{ $sensor->gateway }}" required autocomplete="gateway" autofocus>

                                    @error('gateway')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('ชื่อ') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ $sensor->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="displayname" class="col-md-3 col-form-label text-md-right">{{ __('Display Name') }}</label>

                                <div class="col-md-6">
                                    <input id="displayname" type="text" class="form-control @error('displayname') is-invalid @enderror"
                                        name="displayname" value="{{ $sensor->displayname }}" required autocomplete="displayname" autofocus>

                                    @error('displayname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="coil" class="col-md-3 col-form-label text-md-right">{{ __('Coil') }}</label>

                                <div class="col-md-6">
                                    <input id="coil" type="text" class="form-control @error('coil') is-invalid @enderror"
                                        name="coil" value="{{ $sensor->coil }}" required autocomplete="coil" autofocus>

                                    @error('coil')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="show" class="col-md-3 col-form-label text-md-right">{{ __('Show') }}</label>

                                <div class="col-md-6">
                                    {{ Form::select('show', array('Yes'=>'Yes','No'=>'No'), $sensor->show, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    

                                    @error('show')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="seq" class="col-md-3 col-form-label text-md-right">{{ __('Seq') }}</label>

                                <div class="col-md-6">
                                    <input id="seq" type="number" class="form-control @error('typecolumn') is-invalid @enderror"
                                        name="seq" value="{{ $sensor->seq }}" required autofocus>

                                    @error('seq')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="conditions" class="col-md-3 col-form-label text-md-right">{{ __('Condition RED') }}</label>

                                <div class="col-md-6">
                                    <input id="conditions" type="text" class="form-control @error('conditions') is-invalid @enderror"
                                        name="conditions" value="{{ $sensor->conditions }}" autofocus>

                                    @error('conditions')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="conditions2" class="col-md-3 col-form-label text-md-right">{{ __('Condition Yellow') }}</label>

                                <div class="col-md-6">
                                    <input id="conditions2" type="text" class="form-control @error('conditions2') is-invalid @enderror"
                                        name="conditions2" value="{{ $sensor->conditions2 }}" autofocus>

                                    @error('conditions2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="displaycond" class="col-md-3 col-form-label text-md-right">{{ __('Condition Display') }}</label>

                                <div class="col-md-6">
                                    <input id="displaycond" type="text" class="form-control @error('displaycond') is-invalid @enderror"
                                        name="displaycond" value="{{ $sensor->displaycond }}" autofocus>

                                    @error('displaycond')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-6">
                                    <input id="status" type="text" class="form-control @error('status') is-invalid @enderror"
                                        name="status" value="{{ $sensor->status }}" autofocus>

                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('แก้ไข') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Column Map กับ Excel Scada') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('temp_map_columns.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แก้ไข Column Map กับ Excel Scada') }}</div>

                    <div class="card-body">
                        <form action="{{ route('temp_map_columns.update', $tempmapcolumn->id) }}" method="POST">
                            @csrf

                            @method('PUT')

                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('ชื่อ') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ $tempmapcolumn->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="displayname" class="col-md-3 col-form-label text-md-right">{{ __('Displayname') }}</label>

                                <div class="col-md-6">
                                    <input id="displayname" type="text" class="form-control @error('displayname') is-invalid @enderror"
                                        name="displayname" value="{{ $tempmapcolumn->displayname }}" required autofocus>

                                    @error('displayname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="mapcolumn" class="col-md-3 col-form-label text-md-right">{{ __('Map Column') }}</label>

                                <div class="col-md-6">
                                    <input id="mapcolumn" type="text" class="form-control @error('mapcolumn') is-invalid @enderror"
                                        name="mapcolumn" value="{{ $tempmapcolumn->mapcolumn }}" required autofocus>

                                    @error('mapcolumn')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="displaycond" class="col-md-3 col-form-label text-md-right">{{ __('Display Column') }}</label>

                                <div class="col-md-6">
                                    <input id="displaycond" type="text" class="form-control @error('displaycond') is-invalid @enderror"
                                        name="displaycond" value="{{ $tempmapcolumn->displaycond }}" required autofocus>

                                    @error('displaycond')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="typecolumn" class="col-md-3 col-form-label text-md-right">{{ __('Type Column') }}</label>

                                <div class="col-md-6">
                                    <input id="typecolumn" type="text" class="form-control @error('typecolumn') is-invalid @enderror"
                                        name="typecolumn" value="{{ $tempmapcolumn->typecolumn }}" required autofocus>

                                    @error('typecolumn')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="show" class="col-md-3 col-form-label text-md-right">{{ __('Show') }}</label>

                                <div class="col-md-6">
                                    {{ Form::select('show', array('Yes'=>'Yes','No'=>'No'), $tempmapcolumn->show, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}


                                    @error('show')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="seq" class="col-md-3 col-form-label text-md-right">{{ __('Seq') }}</label>

                                <div class="col-md-6">
                                    <input id="seq" type="number" class="form-control @error('typecolumn') is-invalid @enderror"
                                        name="seq" value="{{ $tempmapcolumn->seq }}" required autofocus>

                                    @error('seq')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="conditions" class="col-md-3 col-form-label text-md-right">{{ __('Condition RED') }}</label>

                                <div class="col-md-6">
                                    <input id="conditions" type="text" class="form-control @error('conditions') is-invalid @enderror"
                                        name="conditions" value="{{ $tempmapcolumn->conditions }}" autofocus>

                                    @error('conditions')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="conditions2" class="col-md-3 col-form-label text-md-right">{{ __('Condition Yellow') }}</label>

                                <div class="col-md-6">
                                    <input id="conditions2" type="text" class="form-control @error('conditions2') is-invalid @enderror"
                                        name="conditions2" value="{{ $tempmapcolumn->conditions2 }}" autofocus>

                                    @error('conditions2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-6">
                                    <input id="status" type="text" class="form-control @error('status') is-invalid @enderror"
                                        name="status" value="{{ $tempmapcolumn->status }}" autofocus>

                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('แก้ไข') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ARD Reports Alert</title>
    <style>
        table {
            border: solid 2px black;
            font-size: 0.75em;
        }

        thead tr th {
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
            background-color: rgb(230, 215, 215);
        }

        tbody tr td {
            border: solid 1px black;
        }

        .redbg {
            background-color: #ff0000;
        }

        .yellowbg {
            background-color: #ffff00;
        }

        .blackbg {
            background-color: #000000;
        }

    </style>
</head>

<body>
    <p><strong>TO..ALL</strong></p>
    <p><strong>รายงานผลการตรวจสอบอุณหภูมิห้องเก็บสินค้าต่างๆ วันที่ {{ $date }} รายละเอียดดังนี้</strong></p>
    <p>
    <table>
        <thead>
            <tr>
                <th>รายละเอียด</th>
                <th>Coil</th>
                <th>Temp SPEC (˚C)</th>
                @foreach ($dataset['timeList'] as $key => $value)
                    <th>{{ $value }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($dataset['allColumnsDatas'] as $ColumnsData)
                <tr>
                    <td>{{ $ColumnsData->displayname }}</td>
                    <td>{{ $ColumnsData->mapcolumn }}</td>
                    <td>{{ $ColumnsData->displaycond }}</td>
                    @foreach ($dataset['timeList'] as $key => $value)
                        @if (isset($dataset['data'][$ColumnsData->id]) && isset($dataset['data'][$ColumnsData->id][$date . ' ' . $value]))
                            @if (!empty($ColumnsData->conditions2))
                                @if (!empty($dataset['data'][$ColumnsData->id][$date . ' ' . $value]))
                                    @php
                                        $strcond = 'return ' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . $ColumnsData->conditions . ';';
                                        $pos = strpos($ColumnsData->conditions2, '<');
                                        if ($pos === false) {
                                            $condexplo = explode('ถึง', $ColumnsData->conditions2);
                                            $strcond = 'return (' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . '>=' . $condexplo[0] . ' && ' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . '<=' . $condexplo[1] . ');';

                                        }
                                    @endphp
                                    @if (eval($strcond))
                                        <td class='yellowbg'>
                                            {{ number_format($dataset['data'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                            @if (isset($dataset['flagData'][$ColumnsData->id][$date . " " . $value]))
                                                ,{{ $dataset['flagData'][$ColumnsData->id][$date . " " . $value] }}
                                            @endif
                                        </td>
                                    @else
                                        @if (!empty($ColumnsData->conditions))
                                            @if (!empty($dataset['data'][$ColumnsData->id][$date . ' ' . $value]))
                                                @php
                                                    $strcond = 'return ' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . $ColumnsData->conditions . ';';
                                                    $pos = strpos($ColumnsData->conditions, '<');
                                                    if ($pos === false) {
                                                        $condexplo = explode('-', $ColumnsData->conditions);
                                                        $strcond = 'return (' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . '>=' . $condexplo[0] . ' && ' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . '<=' . $condexplo[1] . ');';
                                                    }
                                                @endphp
                                                @if (!eval($strcond))
                                                    <td class='redbg'>
                                                        {{ number_format($dataset['data'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                        @if (isset($dataset['flagData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                                    </td>
                                                @else
                                                    <td>{{ number_format($dataset['data'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                        @if (isset($dataset['flagData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                                    </td>
                                                @endif
                                            @else
                                                <td class='blackbg'>-</td>
                                            @endif
                                        @else
                                            <td>{{ number_format($dataset['data'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                @if (isset($dataset['flagData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                            </td>
                                        @endif
                                    @endif
                                @else
                                    <td class='blackbg'>-</td>
                                @endif
                            @else
                                @if (!empty($ColumnsData->conditions))
                                    @if (!empty($dataset['data'][$ColumnsData->id][$date . ' ' . $value]))
                                        @php
                                            $strcond = 'return ' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . $ColumnsData->conditions . ';';
                                            $pos = strpos($ColumnsData->conditions, '<');
                                            if ($pos === false) {
                                                $condexplo = explode('ถึง', $ColumnsData->conditions);
                                                $strcond = 'return (' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . '>=' . $condexplo[0] . ' && ' . $dataset['data'][$ColumnsData->id][$date . ' ' . $value] . '<=' . $condexplo[1] . ');';
                                            }
                                        @endphp
                                        @if (!eval($strcond))
                                            <td class='redbg'>
                                                {{ number_format($dataset['data'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                @if (isset($dataset['flagData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                            </td>
                                        @else
                                            <td>{{ number_format($dataset['data'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                @if (isset($dataset['flagData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                            </td>
                                        @endif
                                    @else
                                        <td class='blackbg'>-</td>
                                    @endif
                                @else
                                    <td>{{ number_format($dataset['data'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                        @if (isset($dataset['flagData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                    </td>
                                @endif
                            @endif

                        @else

                            <td>dd</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach


                    @foreach ($dataset['allColumnsRteDatas'] as $ColumnsData)
                <tr>
                    <td>{{ $ColumnsData->displayname }}</td>
                    <td>{{ $ColumnsData->coil }}</td>
                    <td>{{ $ColumnsData->displaycond }}</td>
                    @foreach ($dataset['timeList'] as $key => $value)
                        @if (isset($dataset['dataRte'][$ColumnsData->id]) && isset($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value]))
                            @if (!empty($ColumnsData->conditions2))
                                @if (!empty($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value]))
                                    @php
                                        $strcond = 'return ' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . $ColumnsData->conditions . ';';
                                        $pos = strpos($ColumnsData->conditions2, '<');
                                        if ($pos === false) {
                                            $condexplo = explode('ถึง', $ColumnsData->conditions2);
                                            $strcond = 'return (' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . '>=' . $condexplo[0] . ' && ' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . '<=' . $condexplo[1] . ');';

                                        }
                                    @endphp
                                    @if (eval($strcond))
                                        <td class='yellowbg'>
                                            {{ number_format($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                            @if (isset($dataset['flagRteData'][$ColumnsData->id][$date . " " . $value]))
                                                ,{{ $dataset['flagRteData'][$ColumnsData->id][$date . " " . $value] }}
                                            @endif
                                        </td>
                                    @else
                                        @if (!empty($ColumnsData->conditions))
                                            @if (!empty($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value]))
                                                @php
                                                    $strcond = 'return ' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . $ColumnsData->conditions . ';';
                                                    $pos = strpos($ColumnsData->conditions, '<');
                                                    if ($pos === false) {
                                                        $condexplo = explode('-', $ColumnsData->conditions);
                                                        $strcond = 'return (' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . '>=' . $condexplo[0] . ' && ' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . '<=' . $condexplo[1] . ');';
                                                    }
                                                @endphp
                                                @if (!eval($strcond))
                                                    <td class='redbg'>
                                                        {{ number_format($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                        @if (isset($dataset['flagRteData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagRteData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                                    </td>
                                                @else
                                                    <td>{{ number_format($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                        @if (isset($dataset['flagRteData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagRteData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                                    </td>
                                                @endif
                                            @else
                                                <td class='blackbg'>-</td>
                                            @endif
                                        @else
                                            <td>{{ number_format($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                @if (isset($dataset['flagRteData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagRteData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                            </td>
                                        @endif
                                    @endif
                                @else
                                    <td class='blackbg'>-</td>
                                @endif
                            @else
                                @if (!empty($ColumnsData->conditions))
                                    @if (!empty($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value]))
                                        @php
                                            $strcond = 'return ' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . $ColumnsData->conditions . ';';
                                            $pos = strpos($ColumnsData->conditions, '<');
                                            if ($pos === false) {
                                                $condexplo = explode('ถึง', $ColumnsData->conditions);
                                                $strcond = 'return (' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . '>=' . $condexplo[0] . ' && ' . $dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value] . '<=' . $condexplo[1] . ');';
                                            }
                                        @endphp
                                        @if (!eval($strcond))
                                            <td class='redbg'>
                                                {{ number_format($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                @if (isset($dataset['flagRteData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagRteData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                            </td>
                                        @else
                                            <td>{{ number_format($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                                @if (isset($dataset['flagRteData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagRteData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                            </td>
                                        @endif
                                    @else
                                        <td class='blackbg'>-</td>
                                    @endif
                                @else
                                    <td>{{ number_format($dataset['dataRte'][$ColumnsData->id][$date . ' ' . $value], 2, '.', ',') }}
                                        @if (isset($dataset['flagRteData'][$ColumnsData->id][$date . " " . $value]))
                                                            ,{{ $dataset['flagRteData'][$ColumnsData->id][$date . " " . $value] }}
                                                        @endif
                                    </td>
                                @endif
                            @endif

                        @else

                            <td>dd</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach

        </tbody>
    </table>
    </p>
    <p>ขอบคุณครับ</p>
    <p>QC Temperature Scada Alert</p>
</body>

</html>

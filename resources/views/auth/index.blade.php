@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('User') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('user.create') }}">สร้าง User</a>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                            <div class="pagination-wrapper"> {!! $user->render('vendor\pagination\bootstrap-4') !!} </div>
                   
                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                            @foreach ($user as $key)
                                <tr>
                                    <td>{{ $key->name }}</td>
                                    <td>{{ $key->email }}</td>
                                    <td>
                                        {{-- <a class="btn btn-info" href="{{ route('user.show', $key->id) }}">แสดง</a> --}}
                                        <a class="btn btn-primary" href="{{ route('user.edit', $key->id) }}">แก้ไข</a>
                                        {{-- <a class="btn btn-danger" href="{{ route('user.destroy', $key->id) }}">ลบ</a> --}}
                                        <form action="{{ route('user.destroy', $key->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                         <div class="pagination-wrapper"> {!! $user->render('vendor\pagination\bootstrap-4') !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
